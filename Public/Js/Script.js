(($) => {
    $.fn.extend({
        AccountPlugin: function() {

            /**
             * Hardcoded values
             * @type {{}}
             */
            this.Init = {

            };

            /**
             * Use to define all plugin selectors
             */
            this.Selector = {
                AccountToggler:      '.account-list .account-toggler',
                AccountRemoveImage:  '.remove-image',
            };

            /**
             * Use to initiate selected objects
             */
            this.JqueryObject = {
                AccountToggler:       $(this.Selector.AccountToggler),
                AccountImageRemove:   $(this.Selector.AccountRemoveImage),
            };

            /**
             * Used to register object methods
             */
            this.Methods = {
                AccountToggler: function(e) {
                    e.preventDefault();
                    let href = $(this).attr('href');
                    let hrefSeparator = href.split('/');
                    let id = hrefSeparator[hrefSeparator.length - 1];

                    let action = 'disable';
                    if ($(this).hasClass('account-disabled')) {
                        action = 'enable';
                    }

                    $.ajax({
                        method: "POST",
                        url: href,
                        data: { id: id, action: action }
                    }).done((data) => {
                        if (data.success == true) {
                            let disabledClasses = 'account-disabled info-style';
                            let enabledClasses  = 'account-enabled important-style';
                            if (action == 'enable') {
                                $('#account-toggler-' + id).text('Disable')
                                    .removeClass(disabledClasses)
                                    .addClass(enabledClasses);
                            } else {
                                $('#account-toggler-' + id).text('Enable')
                                    .removeClass(enabledClasses)
                                    .addClass(disabledClasses);
                            }
                        }
                    });
                },
                AccountImageRemove: function(e) {
                    e.preventDefault();
                    let href = $(this).attr('href');
                    let hrefSeparator = href.split('/');
                    let id = hrefSeparator[hrefSeparator.length - 1];

                    $.ajax({
                        method: "POST",
                        url: href,
                        data: { id: id }
                    }).done((data) => {
                        if (data.success == true) {
                            $(this).closest('.form-group').fadeOut();
                        }
                    });
                },
            };

            /**
             * Re-usable logical methods (do not mix this with general methods
             * function
             *
             * @type {{}}
             */
            this.Services = {

            };

            /**
             * Assigning events
             * @constructor
             */
            this.RegisterEvents = function() {
                this.JqueryObject.AccountToggler.click(this.Methods.AccountToggler);
                this.JqueryObject.AccountImageRemove.click(this.Methods.AccountImageRemove);
            };

            /**
             * Initiating plugin events
             */
            this.RegisterEvents();

            return this;
        }
    });

    $(document).ready(() => {
        $('.edit-account-form, .table account-list').AccountPlugin();
    });
})(jQuery);

(($) => {
    $.fn.extend({
        CommentPlugin: function() {

            /**
             * Hardcoded values
             * @type {{}}
             */
            this.Init = {

            };

            /**
             * Use to define all plugin selectors
             */
            this.Selector = {
                CommentTogglerBtn:      '.comment-list .btn-toggler',
            };

            /**
             * Use to initiate selected objects
             */
            this.JqueryObject = {
                BtnToggler:       $(this.Selector.CommentTogglerBtn),
            };

            /**
             * Used to register object methods
             */
            this.Methods = {
                CommentToggler: function(e) {
                    e.preventDefault();
                    let href = $(this).attr('href');
                    let hrefSeparator = href.split('/');
                    let id = hrefSeparator[hrefSeparator.length - 1];

                    let action = 'disable';
                    if ($(this).hasClass('btn-disabled')) {
                        action = 'enable';
                    }

                    $.ajax({
                        method: "POST",
                        url: href,
                        data: { id: id, action: action }
                    }).done((data) => {
                        if (data.success == true) {
                            let disabledClasses = 'btn-disabled info-style';
                            let enabledClasses  = 'btn-enabled important-style';
                            if (action == 'enable') {
                                $('.comment-list #btn-toggler-' + id).text('Un-publish')
                                    .removeClass(disabledClasses)
                                    .addClass(enabledClasses);
                            } else {
                                $('.comment-list #btn-toggler-' + id).text('Publish')
                                    .removeClass(enabledClasses)
                                    .addClass(disabledClasses);
                            }
                        }
                    });
                },
            };

            /**
             * Re-usable logical methods (do not mix this with general methods
             * function
             *
             * @type {{}}
             */
            this.Services = {

            };

            /**
             * Assigning events
             * @constructor
             */
            this.RegisterEvents = function() {
                this.JqueryObject.BtnToggler.click(this.Methods.CommentToggler);
            };

            /**
             * Initiating plugin events
             */
            this.RegisterEvents();

            return this;
        }
    });

    $(document).ready(() => {
        $('.table.comment-list').CommentPlugin();
    });
})(jQuery);
