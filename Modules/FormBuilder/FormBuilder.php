<?php

namespace Modules;

use Modules\FormBuilder\Field;

class FormBuilder
{
    /**
     * @param string $type
     * @param string $name
     * @param string $value
     * @param array $attributes
     * @param string $label
     * @return string
     */
    public function buildField(string $type, string $name, string $value, array $attributes, ?string $label = '')
    {
        $field = new Field($type, $name, $value, $attributes, $label);
        echo $field->getHtml();
    }
}