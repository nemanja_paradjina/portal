<?php

namespace Modules\FormBuilder;

/**
 * Class Field
 *
 * @package Modules\FormBuilder
 */
class Field
{
    public const INPUT_TYPES = ['password', 'text', 'email', 'hidden', 'file'];
    public const BUTTON_TYPES = ['button', 'submit', 'reset'];
    public const TEXTAREA = ['textarea'];
    public const CHECKBOX = ['checkbox'];

    private $type;
    private $name;
    private $value;
    private $attributes;
    private $label;

    /**
     * TextField constructor.
     * @param string $type
     * @param string $name
     * @param string $value
     * @param array $attributes
     * @param string $label
     */
    public function __construct(string $type, string $name, string $value, array $attributes, string $label)
    {
        $this->type = $type;
        $this->name = $name;
        $this->value = $value;
        $this->attributes = $attributes;
        $this->label = $label;
        $this->reformatAttributes();
    }

    /**
     * Logic for formatting field
     *
     * @return string
     */
    public function getHtml()
    {
        $field = '';

        if (in_array($this->type, self::INPUT_TYPES)) {
            $field = $this->getInput();
        }

        if (in_array($this->type, self::BUTTON_TYPES)) {
            $field = $this->getButton();
        }

        if (in_array($this->type, self::TEXTAREA)) {
            $field = $this->getTextarea();
        }

        if (in_array($this->type, self::CHECKBOX)) {
            $field = $this->getCheckbox();
        }

        return $field;
    }

    /**
     * Generate button html
     *
     * @return string
     */
    private function getButton()
    {
        $field  = '<button type="' . $this->type . '" ';
        foreach ($this->attributes as $key => $attribute) {
            $field .= $key . '="' . $attribute . '" ';
        }
        $field .= 'name="' . $this->name . '" ';
        $field .= 'value="' . $this->value . '" ';
        $field .= '>';
        $field .= $this->value;
        $field .= '</button>';

        return $field;
    }

    /**
     * Generate input html (usually for password, text fields...
     *
     * @return string
     */
    private function getInput()
    {
        $label = !empty($this->label) ? '<label for="' . $this->name . '">' . $this->label . $this->Obligatory() .'</label>' : '';
        $field  = '<input type="' . $this->type . '" ';
        foreach ($this->attributes as $key => $attribute) {
            $field .= $key . '="' . $attribute . '" ';
        }
        $field .= 'name="' . $this->name . '" ';
        $field .= 'value="' . $this->value . '" ';

        $field .= '/>';

        if ('hidden' != $this->type) {
            $field = $this->wrapField($label . $field);
        }

        return $field;
    }

    private function getCheckbox()
    {
        $label = '<label for="' . $this->name . '">';
        $field  = '<input type="' . $this->type . '" ';
        foreach ($this->attributes as $key => $attribute) {
            $field .= $key . '="' . $attribute . '" ';
        }
        $field .= 'name="' . $this->name . '" ';
        if (!empty($this->value)) {
            $field .= 'checked="' . $this->value . '" ';
        }
        $field .= '/>';
        $field .= $this->label . $this->Obligatory() .'</label>';
        $field = $this->wrapField($label . $field);
        return $field;
    }

    private function getTextarea()
    {
        $label = '<label for="' . $this->name . '">' . $this->label . $this->Obligatory() .'</label>';
        $field  = '<textarea ';
        foreach ($this->attributes as $key => $attribute) {
            $field .= $key . '="' . $attribute . '" ';
        }
        $field .= 'name="' . $this->name . '" ';
        $field .= '>';
        $field .= $this->value;
        $field .= '</textarea>';

        $field = $this->wrapField($label . $field);
        return $field;
    }

    /**
     * Add wrapper to fields
     *
     * @param string $field
     * @return string
     */
    private function wrapField(string $field): string
    {
        return '<div class="form-group">' . $field . '</div>';
    }

    /**
     * Add asterisk if field is obligatory
     *
     * @return string
     */
    private function Obligatory()
    {
        if (isset($this->attributes['required']) && true == $this->attributes['required']) {
            return '<span class="required">*</span>';
        }
    }

    private function reformatAttributes(): void
    {
        if (isset($this->attributes['required']) && false == $this->attributes['required']) {
            unset($this->attributes['required']);
        }
    }
}