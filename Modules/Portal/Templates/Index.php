<?php
$counter = 1;
$perRow = 4;
$maxProfiles = count($templateData['data']['portal_profiles']);
?>

<?php foreach($templateData['data']['portal_profiles'] as $profile): ?>
    <?php if (1 == $counter):?>
        <div class="row">
    <?php endif; ?>

    <div class="col-sm-3 profile-thumb-wrapper">
        <a href="/account/<?php print $profile->getAccountId(); ?>">
            <div class="profile-image-wrapper">
                <img src="<?php print $profile->getResizedImage(); ?>" />
            </div>
            <div class="first-last-name">
                <?php print $profile->getFirstName(); ?> <?php print $profile->getLastName(); ?>
            </div>
        </a>
    </div>

    <?php if (0 == $counter % $perRow && $counter != $maxProfiles): ?>
        </div><div class="row">
    <?php endif; ?>
    <?php $counter++; ?>
<?php endforeach; ?>
</div>

