<?php

namespace Modules;

use Modules\Account\Controllers\ProfileController;

/**
 * Class Portal
 *
 * @package Modules
 */
class Portal
{

    private const WENT_WRONG = 'Oops, something went wrong.';
    /**
     * Index page
     */
    public function PagePortal(array $request): array
    {
        $profileController = new ProfileController();
        $profiles = $profileController->getAll(['public' => true]);

        return [
            'template' => __DIR__ . '/Templates/Index.php',
            'request' => $request,
            'data'    => [
                'title' => 'Welcome to the portal',
                'page_title' => 'Welcome to our new portal',
                'portal_profiles' => $profiles,
                'messages' => []
            ]
        ];
    }

    public function PageError(array $request): array
    {
        $messages = $_SESSION['messages'] ?? [];
        unset($_SESSION['messages']);

        return [
            'template' => __DIR__ . '/Templates/Error.php',
            'request' => $request,
            'data'    => [
                'title' => 'Something went wrong',
                'page_title' => self::WENT_WRONG,
                'messages' => $messages
            ]
        ];
    }

}