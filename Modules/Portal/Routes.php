<?php

global $routes;

$routes['/'] = [
    'GET' => 'Modules\Product|PageProducts',
];

$routes['/portal'] = [
    'GET' => 'Modules\Portal|PagePortal',
];

$routes['/error'] = [
    'GET' => 'Modules\Portal|PageError',
];
