<?php
if (!$permissionManager->can('edit_comments')) {
    return;
}
?>

<table class="table comment-list">
    <thead>
    <th scope="col">ID</th>
    <th scope="col">Name</th>
    <th scope="col">Email</th>
    <th scope="col">Comment</th>
    <th scope="col">Type</th>
    <th scope="col">Actions</th>
    </thead>
    <tbody>
    <?php foreach ($templateData['data']['comments'] as $comment): ?>
        <tr>
            <td scope="row"><?php print $comment->getId(); ?></td>
            <td scope="row"><?php print $comment->getName(); ?></td>
            <td scope="row"><?php print $comment->getEmail(); ?></td>
            <td scope="row"><?php print $comment->getComment(); ?></td>
            <td scope="row"><?php print $comment->getEntityType(); ?></td>
            <td scope="row">
                <?php if($comment->getPublished() == 0): ?>
                    <a id="btn-toggler-<?php print $comment->getId(); ?>" href="/api/toggle-comment/<?php print $comment->getId(); ?>" class="btn btn-primary link-button info-style btn-toggler btn-disabled">Publish</a>
                <?php else: ?>
                    <a id="btn-toggler-<?php print $comment->getId(); ?>" href="/api/toggle-comment/<?php print $comment->getId(); ?>" class="btn btn-primary link-button important-style btn-toggler btn-enabled">Un-publish</a>
                <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>