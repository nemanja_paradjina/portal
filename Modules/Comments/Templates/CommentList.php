<div class="comments-wrapper">
    <h3>Comments</h3>

    <?php if (empty($templateData['data']['comments'])): ?>
        <p>There are no comments yet.</p>
    <?php endif; ?>

    <?php foreach ($templateData['data']['comments'] as $comment): ?>
        <div class="comment-wrapper">
            <div class="comment-sender">
                <?php print $comment->getName(); ?>
            </div>
            <div class="comment-body">
                <?php print $comment->getComment(); ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>