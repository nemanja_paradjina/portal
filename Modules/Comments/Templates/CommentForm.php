<form method="post" action="<?php formAction(); ?>" enctype="multipart/form-data" class="edit-product-form">
    <div class="row">
        <div class="col-xs-12">
            <h3>Add new comment</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <?php $FormBuilder->buildField('text', 'name', '', ['class' => 'form-control', 'required' => 'true'], 'Your name'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <?php $FormBuilder->buildField('email', 'email', '', ['class' => 'form-control', 'required' => 'true'], 'Your email address'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <?php $FormBuilder->buildField('textarea', 'comment', '', ['class' => 'form-control'], 'Product Description'); ?>
        </div>
    </div>
    <?php $FormBuilder->buildField('hidden', 'entity_id', $commentEntityId, ['class' => 'form-control', 'required' => 'true'], ''); ?>
    <?php $FormBuilder->buildField('hidden', 'entity_type', $commentEntityType, ['class' => 'form-control', 'required' => 'true'], ''); ?>
    <?php $FormBuilder->buildField('submit', 'create', 'Add comment', ['class' => 'form-control btn btn-primary']); ?>
</form>