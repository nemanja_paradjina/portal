<?php

namespace Modules\Comments\Model;

/**
 * Class Comment
 * @package Modules\Comments\Model
 */
class Comment
{
    use \CreateTrait;
    use \UpdateTrait;

    public $required = [
        'create' => ['name', 'email']
    ];
    public $operation;

    private $table = 'comments';

    private $id;
    private $email;
    private $name;
    private $comment;
    private $entityType;
    private $entityId;
    private $published;
    private $map;


    /**
     * @return mixed
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * @param mixed $operation
     */
    public function setOperation($operation)
    {
        $this->operation = $operation;
    }

    /**
     * @return mixed
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @param mixed $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getEntityType()
    {
        return $this->entityType;
    }

    /**
     * @param mixed $entityType
     */
    public function setEntityType($entityType)
    {
        $this->entityType = $entityType;
    }

    /**
     * @return mixed
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * @param mixed $entityId
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;
    }

    /**
     * Setter for data mapping
     */
    public function setMap(): void
    {
        $this->mapData();
    }

    /**
     * Returns product data map
     *
     * @return array
     */
    public function getMap(): array
    {
        return $this->map;
    }

    /**
     * Map request data field with model elements
     */
    protected function mapData(): void
    {
        $this->map = [
            'id'          => $this->getId(),
            'name'        => $this->getName(),
            'email'       => $this->getEmail(),
            'comment'     => $this->getComment(),
            'entity_type' => $this->getEntityType(),
            'entity_id'   => $this->getEntityId(),
            'published'   => $this->getPublished(),
        ];
    }
}