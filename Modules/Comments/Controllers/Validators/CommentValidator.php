<?php

namespace Modules\Comments\Controllers\Validators;

use Modules\Comments\Model\Comment;

/**
 * Class CommentValidator
 * @package Modules\Comments\Controllers\Validators
 */
class CommentValidator
{
    private $required;
    private $data;
    private $messages = [];
    private $comment;
    private $noPass = false;
    protected $database;

    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
        $this->required = $comment->required[$comment->getOperation()];
        $this->data = $comment->getMap();

        $this->ValidateRequired();
    }

    /**
     * Returns information if model is valid
     *
     * @return bool
     */
    public function Valid(): bool
    {
        return !$this->noPass;
    }

    /**
     * Gets array of generated messages
     *
     * @return mixed
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

    /**
     * Checks if required fields are empty
     */
    private function ValidateRequired(): void
    {
        foreach ($this->data as $key => $field) {
            if (in_array($key, $this->required) && empty($field)) {
                $this->noPass = true;
                $this->messages['alert'][] = 'Field ' . $key . ' cannot be empty.';
            }
        }
    }
}