<?php


namespace Modules\Comments\Controllers;

use Modules\Comments\Controllers\Validators\CommentValidator;
use Modules\Comments\Model\Comment;
use Modules\Db\Database;

/**
 * Class CommentsController
 *
 * @package Modules\Comments\Controllers
 */
class CommentsController
{
    public function getById(int $id)
    {
        $query = 'SELECT * FROM comments WHERE id = ?';
        $db = new Database();
        $data = $db->ExecuteGetQuery($query, [$id]);
        $comments = $this->formatComments($data);

        return $comments;
    }

    /**
     * Getting array of all comments
     *
     * @return array
     */
    public function getAll()
    {
        $query = 'SELECT * FROM comments ORDER BY id DESC';
        $db = new Database();
        $data = $db->ExecuteGetQuery($query, []);
        $comments = $this->formatComments($data);

        return $comments;
    }

    /**
     * Get all published comments for provided entity
     *
     * @param string $type
     * @param int $entityId
     * @return array
     */
    public function getByEntity(string $type, int $entityId)
    {
        $query = 'SELECT * FROM comments WHERE entity_type = ? AND entity_id=? and published = 1';
        $db = new Database();
        $data = $db->ExecuteGetQuery($query, [$type, $entityId]);
        $comments = $this->formatComments($data);

        return $comments;
    }

    /**
     * Preparing formatted comments
     *
     * @param array $data
     * @return array
     */
    private function formatComments(array $data)
    {
        $comments = [];
        foreach ($data as $commentData) {
            $comment = $this->prepareData($commentData);
            $comment->setId($commentData['id']);
            $comment->setMap();
            $comments[] = $comment;
        }

        return $comments;
    }

    /**
     * Method for adding new comment on products
     *
     * @param array $request
     * @return Comment
     */
    public function createComment(array $request): Comment
    {
        $comment = $this->prepareData($request);
        $comment->setOperation('create');
        $comment->setMap();
        $commentValidator = new CommentValidator($comment);

        if ($commentValidator->Valid()) {
            $comment->create();
            return $comment;
        }
    }

    /**
     * Mapping comment data
     *
     * @param array $request
     * @return Comment
     */
    private function prepareData(array $request): Comment
    {
        $comment = new Comment();
        $comment->setEmail($request['email']);
        $comment->setName($request['name']);
        $comment->setComment($request['comment']);
        $comment->setEntityId($request['entity_id']);
        $comment->setEntityType($request['entity_type']);
        $comment->setPublished(isset($request['published']) ? $request['published'] : 0);

        return $comment;
    }
}