<?php

global $routes;

$routes['/comments'] = [
    'GET'  => 'Modules\Comments|PageCommentManager',
];

$routes['/api/toggle-comment/%'] = [
    'POST' => 'Modules\Comments|ApiCommentPublishing',
];