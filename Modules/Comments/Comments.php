<?php


namespace Modules;

use Modules\Account\Controllers\AccountController;
use Modules\Comments\Controllers\CommentsController;

/**
 * Class Comments
 * @package Modules
 */
class Comments
{
    private $accountController;
    private $messages = [];

    public const MANAGING_COMMENTS_NOT_ALLOWED = 'You are not allowed to edit comments.';
    public const MANAGING_COMMENTS_TITLE = 'Publish comments';

    public function __construct()
    {
        $this->accountController = new AccountController();
    }

    /**
     * JSON api for publishing comments
     *
     * @param array $request
     * @return false|string|void
     */
    public function ApiCommentPublishing(array $request)
    {
        $this->accountController->CheckPermissions('edit_comments', null, self::MANAGING_COMMENTS_NOT_ALLOWED);

        $commentController = new CommentsController();
        $comment = $commentController->getById($request['id']);

        if (!isset($comment[0])) {
            return;
        }
        $success = false;
        $commentModel = $comment[0];

        if ($request['action'] == 'enable') {
            $commentModel->setPublished(1);
        } else {
            $commentModel->setPublished(0);
        }
        $commentModel->setMap();
        $success = $commentModel->update();

        return json_encode([
            'success' => $success,
        ]);
    }

    /**
     * Administer comments page
     *
     * @param array $request
     * @return array
     */
    public function PageCommentManager(array $request)
    {
        $this->accountController->CheckPermissions('edit_comments', null, self::MANAGING_COMMENTS_NOT_ALLOWED);
        $template = __DIR__ . '/Templates/Comments.php';
        $title = getTitle(self::MANAGING_COMMENTS_TITLE);
        $pageTitle = self::MANAGING_COMMENTS_TITLE;
        $commentController = new CommentsController();
        $commentList = $commentController->getAll();

        return [
            'template' => $template,
            'request' => $request,
            'data'    => [
                'title' => $title,
                'page_title' => $pageTitle,
                'comments' => $commentList ?? [],
                'messages' => $this->messages
            ]
        ];
    }
}