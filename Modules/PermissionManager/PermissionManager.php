<?php

namespace Modules;

use Modules\Account\Model\Account;
/**
 * Class PermissionManager
 * @package Modules
 */
class PermissionManager
{
    private $permissions = [];

    public function __construct()
    {
        $this->permissions = [
            'logout'                => ['authenticated'],
            'view_own_profile'      => ['authenticated'],
            'edit_comments'         => ['admin'],
            'add_product'           => ['admin'],
            'list_profiles'         => ['admin'],
            'add_account'           => ['admin'],
            'create_account'        => ['anonymous'],
            'edit_account'          => ['own', 'admin'],
            'view_account_info'     => ['admin'],
            'configure_permissions' => ['admin']
        ];
    }

    /**
     * Returning access information
     *
     * @param string $permission
     * @return bool
     */
    public function can(string $permission, $argument = null): bool
    {
        $allowedRoles = $this->permissions[$permission];
        $result = false;
        foreach($allowedRoles as $allowedRole) {
            if (null !== $argument && $allowedRole == 'own') {
                $result = $this->$allowedRole($argument);
            }

            if (null === $argument) {
                $result = $this->$allowedRole();
            }

            if ($result == true) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if user is anonymous
     *
     * @return bool
     */
    private function anonymous(): bool
    {
        if (!isset($_SESSION['account'])) {
            return true;
        }
        return false;
    }

    /**
     * Checks if is authenticated
     *
     * @return bool
     */
    private function authenticated(): bool
    {
        $account = new Account();

        if (!isset($_SESSION['account'])) {
            return false;
        }

        $account = $account->get($_SESSION['account']->getId());

        if (null == $account || $account->getActive() == 0) {
            unset($_SESSION['account']);
            return false;
        }

        return !$this->anonymous();
    }

    /**
     * Checks if is admin
     *
     * @return bool
     */
    private function admin(): bool
    {
        if (!isset($_SESSION['account']) || null == $_SESSION['account']->getId()) {
            unset($_SESSION['account']);
            return false;
        }

        $account = new Account();
        $account = $account->get($_SESSION['account']->getId());
        $_SESSION['account'] = $account;
        if (!$this->anonymous() && $account->getIsAdmin() === 1) {
            return true;
        }
        return false;
    }

    /**
     * Comparison of account id with session account id to determine if user can manage it
     *
     * @param array|null $arguments
     * @return bool
     */
    public function own(int $argument): bool
    {
        if ($this->authenticated() && ($_SESSION['account']->getId() == $argument || $this->admin())) {
            return true;
        }
        return false;
    }
}