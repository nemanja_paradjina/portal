<?php

namespace Modules;

use Modules\RequestHandler\Traits\RequestHandlerTrait;

/**
 * Class RequestHandler
 *
 * @package Modules
 */
class RequestHandler
{
    use RequestHandlerTrait;

    private $ajax;
    private $data;

    public function __construct()
    {
        $this->data = $this->getDataByRequest();
    }

    /**
     * Main method for resolving requests
     *
     * @return mixed
     */
    public function Resolve()
    {
        return $this->ExecuteRequest();
    }

    /**
     * Checks if it is an API call
     *
     * @return bool
     */
    public function isApi(): bool
    {
        if (strpos($this->data['path'], '/api/') !== false) {
            return true;
        }

        return false;
    }

    /**
     * Handling request, connecting routes to the URL request
     *
     * @param $data
     * @return mixed
     */
    private function ExecuteRequest()
    {
        global $routes;

        $type = $this->data['type'];
        $path = $this->data['path'];

        $urlParameters = $this->CleanUrlData($this->data);
        $path  = $this->ExtractUrlTemplate($path);

        if (empty($urlParameters)) {
            $urlParameters = $this->getArguments();
        }

        $urlParameters = $this->FormatUrlParameters($urlParameters);

        if (isset($routes[$path]) && isset($routes[$path][$type])) {
            $requestMethodData = $routes[$path][$type];

            $requestMethodClass = explode('|', $requestMethodData);
            $className = '\\' . $requestMethodClass[0];
            $methodName = $requestMethodClass[1];
            $requestClass = new $className();

            return $requestClass->$methodName($urlParameters);
        }

        Redirect('/');
    }

    /**
     * Cleaning textual data
     *
     * @param $params
     * @return mixed
     */
    private function FormatUrlParameters($params)
    {
        if (is_array($params)) {
            foreach ($params as $key => $param) {
                if ($key != 'files') {
                    $params[$key] = htmlentities(strip_tags($param));
                }

                if ($param == 'on') {
                    $params[$key] = 1;
                }

                if ($param == 'off') {
                    $params[$key] = 0;
                }
            }
        }

        return $params;
    }
}
