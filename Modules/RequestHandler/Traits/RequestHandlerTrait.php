<?php

namespace Modules\RequestHandler\Traits;

/**
 * Trait RequestHandlerTrait
 *
 * @package Modules\RequestHandler\Traits
 */
trait RequestHandlerTrait
{
    private $arguments;

    /**
     * Based on the current request, we need to retrieve data
     *
     * @return array|mixed
     */
    public function getDataByRequest(): array
    {
        $RequestMethod = $_SERVER['REQUEST_METHOD'];

        $data = [];
        switch($RequestMethod) {
            case 'GET':
                $data = filter_input_array(INPUT_GET, $_GET);
                $data['path'] = '/' . $data['path'];
            break;
            case 'POST':
                $data = filter_input_array(INPUT_POST, $_POST);
                $data['path'] = $_SERVER['REQUEST_URI'];
                if (isset($_FILES)) {
                    $data['files'] = $_FILES;
                }
            break;
        }

        $data['type'] = $RequestMethod;

        return $data;
    }

    /**
     * Getting URL query without system reserved variables
     *
     * @param array $urlData
     * @return array
     */
    private function CleanUrlData(array $urlData): array
    {
        $forbiddenData = ['type', 'path'];
        foreach ($forbiddenData as $dataString) {
            if (array_key_exists($dataString, $urlData)) {
                unset($urlData[$dataString]);
            }
        }

        return $urlData;
    }

    /**
     * Extracting numeric arguments from a path
     *
     * @param string $path
     * @return array
     */
    private function getArguments(): ?array
    {
        return $this->arguments;
    }

    /**
     * Get template of the current URL
     *
     * @param string $path
     * @return string
     */
    private function ExtractUrlTemplate(string $path)
    {
        $pathSeparation = explode('/', $path);
        global $routes;
        $allRoutes = array_keys($routes);

        foreach ($allRoutes as $routeUrl) {
            $pathIsValid = $this->matchPath($pathSeparation, $routeUrl);

            if ($pathIsValid == true) {
                return $routeUrl;
            }
        }

        return '';
    }

    /**
     * Matching routes with provided path
     *
     * @param array $pathSeparation
     * @param string $routeUrl
     * @return bool
     */
    public function matchPath(array $pathSeparation, string $routeUrl): bool
    {
        $routeSeparation = explode('/', $routeUrl);
        $arguments = [];

        foreach ($routeSeparation as $key => $routePart) {
            if ('%' == $routePart && !empty($pathSeparation[$key])) {
                $routeSeparation[$key] = $pathSeparation[$key];
                if (isset($routeSeparation[$key - 1])) {
                    $arguments[$routeSeparation[$key - 1]] = $routeSeparation[$key];
                }
            }
        }

        if ($routeSeparation == $pathSeparation) {
            $this->arguments = $arguments;
            return true;
        }

        return false;
    }
}
