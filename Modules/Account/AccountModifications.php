<?php

namespace Modules\Account;

use Modules\Account\Controllers\AccountController;
use Modules\Account\Controllers\ProfileController;
use Modules\Account\Controllers\Validators\AccountValidator;
use Modules\Account\Controllers\Validators\ProfileValidator;
use Modules\Account\Model\Account as AccountModel;
use Modules\Account\Model\Profile;
use Modules\EmailEngine;
use Modules\Token;

/**
 * Class AccountModifications
 *
 * @package Modules\Account
 */
class AccountModifications
{
    private const ACCOUNT_UPDATED = 'Account updated successfully';

    private const EDIT_ACCOUNT_PAGE_TITLE = 'Edit Account';
    private const ADD_ACCOUNT_PAGE_TITLE = 'Add Account';
    private const ACCOUNT_UPDATE_NOT_ALLOWED = 'You don\'t have permission to modify this account';
    private const ACCOUNT_CREATE_NOT_ALLOWED = 'You don\'t have permission to create account';
    private const CHANGE_PASSWORD_TITLE = 'Change password';
    private const CHANGED_PASSWORD = 'Password changed. You may now <a href="/account/login">login</a> to continue.';
    private const CHANGED_PASSWORD_TITLE = 'Password changed';
    private const TOKEN_EXPIRED = 'Token expired. You most likely clicked on the link in your mailbox.';
    private const RESET_PASSWORD_TITLE = 'Reset password';
    private const CHECK_MAILBOX = 'Please check your mailbox for further instructions.';
    private const ACCOUNT_NOT_FOUND = 'Account not found, please check if email is properly typed.';
    private const EMAIL_RESET_PASSWORD_SUBJECT = 'PORTAL: Password change';
    private const EMAIL_RESET_PASSWORD_BODY = 'You requested password change. In order to change you password, please click on the following <a href="%s/account/change-password/%s">link</a>.';

    private $accountController;
    private $profileController;
    private $messages = [];

    public function __construct()
    {
        $this->accountController = new AccountController();
        $this->profileController = new ProfileController();
    }

    /**
     * Display account edit form page
     *
     * @param array $request
     * @return array
     */
    public function PageEditAccount(array $request): array
    {
        $this->accountController->CheckPermissions('edit_account', $request['account'], self::ACCOUNT_UPDATE_NOT_ALLOWED);
        $account = new AccountModel();
        $account = $account->get($request['account']);
        $profile = $this->profileController->getOne($request['account'], 'account_id');

        $fields = array_merge($account->getMap(), $profile->getMap());

        $template = __DIR__ . '/Templates/EditAccount.php';
        $title = getTitle(self::EDIT_ACCOUNT_PAGE_TITLE);
        $pageTitle = self::EDIT_ACCOUNT_PAGE_TITLE;

        return [
            'template' => $template,
            'request' => $request,
            'data'    => [
                'title' => $title,
                'page_title' => $pageTitle,
                'fields' => $fields,
                'messages' => $this->messages,
                'action_type' => 'update'
            ]
        ];
    }

    /**
     * Updating account
     *
     * @param array $request
     * @return array
     */
    public function UpdateAccount(array $request): array
    {
        $this->accountController->CheckPermissions('edit_account', $request['account_id'], self::ACCOUNT_UPDATE_NOT_ALLOWED);
        if (isset($request['files']) && !empty($request['files']['image_source']['tmp_name'])) {
            $profile = $this->profileController->getOne($request['account_id'], 'account_id');
            $this->profileController->removeImages($profile);
        }

        $accountType = 'update';
        $files = $this->profileController->HandleFiles($request);

        if (!empty($files)) {
            $request = array_merge($request, $files);
        }

        $account = $this->accountController->prepareStoreAccountData($request);
        $account->setOperation('update');
        $accountValidator = new AccountValidator($account);

        $profile = new Profile($request, 'update');

        $messages = array_merge($accountValidator->getMessages(), $profile->Messages());
        $messages = array_merge($this->messages, $messages);

        if ($accountValidator->Valid() && $profile->Valid() && $files !== NULL) {
            if ($account->Update() && $profile->Update()) {
                $messages['success'][] = self::ACCOUNT_UPDATED;
                $account->ResetSession();
            }
        }

        $template = __DIR__ . '/Templates/EditAccount.php';
        $title = getTitle(self::EDIT_ACCOUNT_PAGE_TITLE);
        $pageTitle = self::EDIT_ACCOUNT_PAGE_TITLE;

        if (empty($profile->getOriginalImage()) || empty($profile->getResizedImage())) {
            $oldProfile = $this->profileController->getOne($request['account_id'], 'account_id');
            $profile->setOriginalImage($oldProfile->getOriginalImage());
            $profile->setResizedImage($oldProfile->getResizedImage());
            $profile->setMap();
        }

        $fields = array_merge($account->getMap(), $profile->getMap());

        return [
            'template' => $template,
            'request' => $request,
            'data'    => [
                'title' => $title,
                'page_title' => $pageTitle,
                'fields' => $fields,
                'messages' => $messages,
                'action_type' => $accountType,
            ]
        ];
    }

    /**
     * Form for account creation
     *
     * @param array $request
     * @return array
     */
    public function PageAccountCreate(array $request)
    {
        $this->accountController->CheckPermissions('add_account', null, self::ACCOUNT_CREATE_NOT_ALLOWED);

        $template = __DIR__ . '/Templates/EditAccount.php';
        $title = getTitle(self::ADD_ACCOUNT_PAGE_TITLE);
        $pageTitle = self::ADD_ACCOUNT_PAGE_TITLE;

        return [
            'template' => $template,
            'request' => $request,
            'data'    => [
                'title' => $title,
                'page_title' => $pageTitle,
                'fields' => [],
                'messages' => $this->messages,
                'action_type' => 'create',
            ]
        ];
    }

    /**
     * Storing account
     *
     * @param AccountFormValidate $request
     * @return array
     */
    public function StoreAccount(array $request): array
    {
        $this->accountController->CheckPermissions('add_account', null, self::ACCOUNT_CREATE_NOT_ALLOWED);
        $data = $this->accountController->storeAccount($request);

        $template = __DIR__ . '/Templates/EditAccount.php';
        $title = getTitle(self::ADD_ACCOUNT_PAGE_TITLE);
        $pageTitle = self::ADD_ACCOUNT_PAGE_TITLE;
        $messages = array_merge($this->messages, $data['messages']);

        return [
            'template' => $template,
            'request' => $request,
            'data'    => [
                'title' => $title,
                'page_title' => $pageTitle,
                'fields' => $data['fields'] ?? [],
                'messages' => $messages,
                'action_type' => 'create',
            ]
        ];
    }

    /**
     * API method for ajax, toggle account if disabled or enabled
     *
     * @param array $request
     * @return false|string
     */
    public function ApiToggleAccount(array $request)
    {
        $this->accountController->CheckPermissions('edit_account', $request['id'], self::ACCOUNT_UPDATE_NOT_ALLOWED);
        $account = new AccountModel();
        $account = $account->get($request['id']);

        if ($request['action'] == 'enable') {
            $account->setActive(1);
        } else {
            $account->setActive(0);
        }
        $success = false;

        $account->setOperation('update');
        // If there is no password it wouldn't hash again
        $account->setPassword('');
        $account->setPasswordRepeat('');
        $account->setMap();
        $accountValidator = new AccountValidator($account);

        if ($accountValidator->Valid() && $account->Update()) {
            $success = true;
        }

        return json_encode([
            'success' => $success,
        ]);
    }

    /**
     * Removing image from profile
     *
     * @param $request
     * @return false|string
     */
    public function ApiRemoveImage($request)
    {
        $this->accountController->CheckPermissions('edit_account', $request['id'], self::ACCOUNT_UPDATE_NOT_ALLOWED);
        $profile = $this->profileController->getOne($request['id'], 'account_id');
        $profile = $this->profileController->removeImages($profile);
        $success = false;
        $profileValidator = new ProfileValidator($profile);

        if ($profileValidator->Valid() && $profile->Update()) {
            $success = true;
        }

        return json_encode([
            'success' => $success,
        ]);
    }

    /**
     * Change password form and submit
     *
     * @param array $request
     * @return array
     */
    public function PageChangePassword(array $request)
    {
        $title = getTitle(self::CHANGE_PASSWORD_TITLE);
        $pageTitle = self::CHANGE_PASSWORD_TITLE;
        $template = __DIR__ . '/Templates/ChangePasswordForm.php';

        if (isset($request['change-password-submit'])) {
            $token = new Token();
            $tokenData = $token->getByHash($request['change-password-hash']);

            if (null != $tokenData) {
                $accountId = $tokenData->getAccountId();
                $account = new AccountModel();
                $account = $account->get($accountId);
                $account->setPassword(md5($request['password']));
                $account->setPasswordRepeat(md5($request['password_repeat']));
                $account->setOperation('update');
                $account->setMap();
                $accountValidator = new AccountValidator($account);
                if (true === $accountValidator->Valid()) {
                    $account->Update();
                    $token->remove($tokenData->getId());
                    $title = getTitle(self::CHANGED_PASSWORD_TITLE);
                    $pageTitle = self::CHANGED_PASSWORD_TITLE;
                    $this->messages['success'][] = self::CHANGED_PASSWORD;
                    $template = '';
                } else {
                    $this->messages = array_merge($accountValidator->getMessages(), $this->messages);
                }
            } else {
                $this->messages['alert'][] = self::TOKEN_EXPIRED;
            }
        }

        $fields = [
            'change_password_hash' => $request['change-password'] ?? $request['change-password-hash'],
        ];

        return [
            'template' => $template ?? '',
            'request' => $request,
            'data'    => [
                'title' => $title,
                'page_title' => $pageTitle,
                'fields'     => $fields ?? [],
                'messages' => $this->messages,
            ]
        ];
    }

    /**
     * Password reset request
     *
     * @param array $request
     * @return array
     */
    public function PageResetPassword(array $request)
    {
        if (!empty($request)) {
            $account = $this->accountController->getByEmail($request);
            if ($account != false) {
                $token = new Token();
                $hash = $token->generateToken($account, 'passwordReset')->get();
                $body = sprintf(self::EMAIL_RESET_PASSWORD_BODY, SITE_URL, $hash);
                $email = new EmailEngine($account->getEmail(), SITE_EMAIL, self::EMAIL_RESET_PASSWORD_SUBJECT, $body);
                $email->send();
                $this->messages['info'][] = self::CHECK_MAILBOX;
                $template = '';
            } else {
                $template = __DIR__ . '/Templates/ForgotPasswordForm.php';
                $this->messages['alert'][] = self::ACCOUNT_NOT_FOUND;
            }
        } else {
            $template = __DIR__ . '/Templates/ForgotPasswordForm.php';
        }

        $title = getTitle(self::RESET_PASSWORD_TITLE);
        $pageTitle = self::RESET_PASSWORD_TITLE;

        return [
            'template' => $template ?? '',
            'request' => $request,
            'data'    => [
                'title' => $title,
                'page_title' => $pageTitle,
                'fields'     => $fields ?? [],
                'messages' => $this->messages,
            ]
        ];
    }
}