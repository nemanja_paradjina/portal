<?php  if ($permissionManager->can('add_account')):  ?>
    <a href="/account/add" class="btn btn-primary link-button add-button">Add Account</a>
<?php endif; ?>

<table class="table account-list">
    <thead>
        <th scope="col">Image</th>
        <th scope="col">ID</th>
        <th scope="col">First name</th>
        <th scope="col">Last name</th>
        <th scope="col">Description</th>
        <th scope="col">Public</th>
        <th scope="col">Actions</th>
    </thead>
    <tbody>
        <?php foreach ($templateData['data']['accountList'] as $account): ?>
            <tr>
                <td scope="row"><img class="table-image" src="<?php print $account->getProfile()->getResizedImage(); ?>"/></td>
                <td scope="row"><?php print $account->getProfile()->getAccountId(); ?></td>
                <td scope="row"><?php print $account->getProfile()->getFirstName(); ?></td>
                <td scope="row"><?php print $account->getProfile()->getLastName(); ?></td>
                <td scope="row"><?php print $account->getProfile()->getDescription(); ?></td>
                <td scope="row"><?php print $account->getProfile()->getPublicProfile() == 1 ? 'Yes' : 'No'; ?></td>
                <td scope="row">
                    <a href="/account/<?php print $account->getProfile()->getAccountId(); ?>/edit" class="btn btn-primary link-button">Edit</a>
                    <?php if($account->getActive() == 0): ?>
                        <a id="account-toggler-<?php print $account->getId(); ?>" href="/api/toggle-account/<?php print $account->getId(); ?>" class="btn btn-primary link-button info-style account-toggler account-disabled">Enable</a>
                    <?php else: ?>
                        <a id="account-toggler-<?php print $account->getId(); ?>" href="/api/toggle-account/<?php print $account->getId(); ?>" class="btn btn-primary link-button important-style account-toggler account-enabled">Disable</a>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>