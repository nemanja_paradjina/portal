<form method="post" action="<?php formAction(); ?>" class="forgot-password-form">
    <?php $FormBuilder->buildField('email', 'email', $templateData['data']['fields']['email'] ?? '', ['class' => 'form-control', 'required' => 'true'], 'E-mail'); ?>
    <?php $FormBuilder->buildField('submit', 'resetpassword', 'Reset password', ['class' => 'form-control btn btn-primary']); ?>
</form>