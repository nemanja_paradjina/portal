<form method="post" action="<?php formAction(); ?>">
    <?php $FormBuilder->buildField('text', 'username', '', ['class' => 'form-control', 'required' => 'true'], 'Username'); ?>
    <?php $FormBuilder->buildField('password', 'password', '', ['class' => 'form-control', 'required' => 'true'], 'Password'); ?>
    <?php $FormBuilder->buildField('submit', 'login', 'Login', ['class' => 'form-control btn btn-primary']); ?>
    <div class="form-group password-reset">Forgot your password? Click here to <a href="/account/password-reset">reset</a>.</div>
</form>
