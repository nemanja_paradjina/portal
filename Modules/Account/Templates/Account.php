<?php  if ($permissionManager->can('edit_account', $templateData['data']['fields']['id'])):  ?>
    <a class="edit-account btn btn-primary link-button" href="/account/<?php print $templateData['data']['fields']['id']; ?>/edit">Edit</a>
<?php endif; ?>

<div class="row">
    <div class="col-sm-12 profile-data">
        <?php  if ($permissionManager->can('view_account_info')):  ?>
            <h2>Account information</h2>

            <table class="table">
                <tr>
                    <th scope="row">ID</th>
                    <td><?php print $templateData['data']['fields']['id']; ?></td>
                </tr>
                <tr>
                    <th scope="row">Username:</th>
                    <td><?php print $templateData['data']['fields']['username']; ?></td>
                </tr>
                <tr>
                    <th scope="row">Email:</th>
                    <td><?php print $templateData['data']['fields']['email']; ?></td>
                </tr>
            </table>
        <?php endif; ?>

        <?php if (isset($templateData['data']['fields'])): ?>
            <h2>Profile information</h2>

            <table class="table">
                <tr>
                    <th scope="row">Image</th>
                    <td><img class="table-image" src="<?php print $templateData['data']['fields']['image_resized']; ?>" /></td>
                </tr>
                <tr>
                    <th scope="row">Full Name</th>
                    <td><?php print $templateData['data']['fields']['first_name'] . ' ' . $templateData['data']['fields']['last_name']; ?></td>
                </tr>
                <tr>
                    <th scope="row">Personal Description</th>
                    <td><?php print $templateData['data']['fields']['description']; ?></td>
                </tr>
            </table>
        <?php endif; ?>
    </div>
</div>
