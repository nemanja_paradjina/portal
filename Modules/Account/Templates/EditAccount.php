<?php
    switch($templateData['data']['action_type']) {
        case 'create':
            $buttonValue = 'Create';
            $buttonName = 'create';
            $passwordRequired = true;
        break;
        default:
            $buttonValue = 'Update';
            $buttonName = 'update';
            $passwordRequired = false;
        break;
    }

    $toggle = [
        1 => true,
        0 => false
    ];

?>

<form method="post" action="<?php formAction(); ?>" enctype="multipart/form-data" class="edit-account-form">
    <div class="row">
        <div class="col-xs-12">
            <h2>Account Information</h2>
        </div>
        <div class="col-xs-6">
            <?php $FormBuilder->buildField('text', 'username', $templateData['data']['fields']['username'] ?? '', ['class' => 'form-control', 'required' => 'true'], 'Username'); ?>
            <?php $FormBuilder->buildField('email', 'email', $templateData['data']['fields']['email'] ?? '', ['class' => 'form-control', 'required' => 'true'], 'E-mail'); ?>
        </div>
        <div class="col-xs-6">
            <?php $FormBuilder->buildField('password', 'password', '', ['class' => 'form-control', 'required' => $passwordRequired], 'Password'); ?>
            <?php $FormBuilder->buildField('password', 'password_repeat', '', ['class' => 'form-control', 'required' => $passwordRequired], 'Repeat Password'); ?>
        </div>
        <div class="col-xs-6 inline-checkboxes">
            <?php if ($permissionManager->can('configure_permissions')): ?>
                <?php $FormBuilder->buildField('checkbox', 'active', $toggle[$templateData['data']['fields']['active'] ?? 0], ['class' => 'form-control account-active', 'data-toggle' => 'toggle', 'data-on' => 'Yes', 'data-off' => 'No'], 'Is Active'); ?>
            <?php endif; ?>
            <?php if ($permissionManager->can('configure_permissions')): ?>
                <?php $FormBuilder->buildField('checkbox', 'admin', $toggle[$templateData['data']['fields']['isAdmin'] ?? 0], ['class' => 'form-control account-admin', 'data-toggle' => 'toggle', 'data-on' => 'Yes', 'data-off' => 'No'], 'Is Admin'); ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h2>Profile Information</h2>
        </div>
        <div class="col-xs-6">
            <?php $FormBuilder->buildField('text', 'first_name', $templateData['data']['fields']['first_name'] ?? '', ['class' => 'form-control', 'required' => 'true'], 'First Name'); ?>
            <?php $FormBuilder->buildField('text', 'last_name', $templateData['data']['fields']['last_name'] ?? '', ['class' => 'form-control', 'required' => 'true'], 'Last Name'); ?>
            <?php $FormBuilder->buildField('checkbox', 'public', $toggle[$templateData['data']['fields']['public'] ?? 0], ['class' => 'form-control account-public', 'data-toggle' => 'toggle', 'data-on' => 'Yes', 'data-off' => 'No'], 'Public Profile'); ?>
        </div>
        <div class="col-xs-6">
            <?php if (!empty($templateData['data']['fields']['image_resized'])): ?>
                <div class="form-group image-wrapper">
                    <label>Current Image</label>
                    <div>
                        <img class="table-image" src="<?php print $templateData['data']['fields']['image_resized']; ?>" />
                        <a href="/api/remove-image/<?php print $templateData['data']['fields']['id']; ?>" class="remove-image form-control btn btn-primary">Remove</a>
                    </div>
                </div>
            <?php endif; ?>

            <?php $FormBuilder->buildField('file', 'image_source', $templateData['data']['fields']['image_source'] ?? '', ['class' => 'form-control'], 'Profile Image'); ?>
        </div>
        <div class="col-xs-12">
            <?php $FormBuilder->buildField('textarea', 'description', $templateData['data']['fields']['description'] ?? '', ['class' => 'form-control'], 'Personal Description'); ?>
            <?php if ('update' == $templateData['data']['action_type']): ?>
                <?php $FormBuilder->buildField('hidden', 'account_id', $templateData['data']['fields']['id'], ['class' => 'form-control', 'required' => 'true'], ''); ?>
            <?php endif; ?>
        </div>
    </div>
    <?php $FormBuilder->buildField('submit', $buttonName, $buttonValue, ['class' => 'form-control btn btn-primary']); ?>
</form>