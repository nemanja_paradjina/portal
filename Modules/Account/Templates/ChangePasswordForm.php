<form method="post" action="<?php formAction(); ?>" class="change-password-account-form">
    <h2>Enter new password</h2>
    <?php $FormBuilder->buildField('password', 'password', '', ['class' => 'form-control', 'required' => true], 'Password'); ?>
    <?php $FormBuilder->buildField('password', 'password_repeat', '', ['class' => 'form-control', 'required' => true], 'Repeat Password'); ?>
    <?php $FormBuilder->buildField('hidden', 'change-password-hash', $templateData['data']['fields']['change_password_hash'] ?? '', ['class' => 'form-control', 'required' => true], 'Repeat Password'); ?>
    <?php $FormBuilder->buildField('submit', 'change-password-submit', 'Change password', ['class' => 'form-control btn btn-primary']); ?>
</form>