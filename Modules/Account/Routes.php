<?php

global $routes;

/**
 * Session initializers
 */
$routes['/account/login'] = [
    'GET'  => 'Modules\Account\AccountLogin|PageLogin',
    'POST' => 'Modules\Account\AccountLogin|PageLogin'
];
$routes['/account/logout'] = [
    'GET' => 'Modules\Account\AccountLogout|PageLogout',
];

/**
 * Account create / update
 */
$routes['/account/add'] = [
    'GET' => 'Modules\Account\AccountModifications|PageAccountCreate',
    'POST' => 'Modules\Account\AccountModifications|StoreAccount'
];
$routes['/account/%/edit'] = [
    'GET' => 'Modules\Account\AccountModifications|PageEditAccount',
    'POST' => 'Modules\Account\AccountModifications|UpdateAccount'
];
$routes['/api/toggle-account/%'] = [
    'POST' => 'Modules\Account\AccountModifications|ApiToggleAccount',
];
$routes['/api/remove-image/%'] = [
    'POST' => 'Modules\Account\AccountModifications|ApiRemoveImage',
];
$routes['/account/change-password/%'] = [
    'GET' => 'Modules\Account\AccountModifications|PageChangePassword',
    'POST' => 'Modules\Account\AccountModifications|PageChangePassword',
];
$routes['/account/password-reset'] = [
    'GET' => 'Modules\Account\AccountModifications|PageResetPassword',
    'POST' => 'Modules\Account\AccountModifications|PageResetPassword',
];

/**
 * Account registration
 */
$routes['/account/register'] = [
    'GET' => 'Modules\Account\AccountRegistration|PageAccountRegister',
    'POST' => 'Modules\Account\AccountRegistration|StoreAccountRegistration'
];
$routes['/account/activation-required'] = [
    'GET' => 'Modules\Account\AccountRegistration|PageAccountActivationRequired',
];
$routes['/account/activate/%'] = [
    'GET' => 'Modules\Account\AccountRegistration|PageAccountActivate',
];

/**
 * Administration pages
 */
$routes['/account/list'] = [
    'GET' => 'Modules\Account\AccountList|PageAccountList',
];

/**
 * Preview pages
 */
$routes['/account/%'] = [
    'GET' => 'Modules\Account|PageProfile',
];
