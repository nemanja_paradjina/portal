<?php


namespace Modules\Account\Model;

use Modules\Account\Controllers\Validators\AccountValidator;
use Modules\Account\Abstraction\ModelDefinition;
use Modules\Db\Database;

/**
 * Class Account
 *
 * @package Modules\Account\Model
 */
class Account extends ModelDefinition
{
    private $username;
    private $email;
    private $active;
    private $id;
    private $isAdmin;
    private $password;
    private $passwordRepeat;
    private $map = [];
    private $profile;

    public $operation;

    public $required = [
        'create' => ['username', 'email', 'password', 'password_repeat', 'first_name', 'last_name'],
        'update' => ['username', 'email', 'first_name', 'last_name']
    ];


    /**
     * Account creation method
     *
     * @return bool|mixed
     */
    public function Create()
    {
        try {
            $userQuery = 'INSERT INTO users(email, username, password, active, admin) VALUES(?, ?, ?, ?, ?)';
            $userData = [$this->getEmail(), $this->getUsername(), $this->getPassword(), $this->getIsAdmin(), $this->getIsAdmin()];
            $database = new Database();

            return $database->ExecuteStoreQuery($userQuery, $userData);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Storing update for account data
     *
     * @return bool
     */
    public function Update()
    {
        $passwordQuery = '';
        $activeQuery = ', active=? ';
        $adminQuery = ', admin=? ';

        $userData = [$this->getEmail(), $this->getUsername(), $this->getPassword(), $this->getActive(), $this->getIsAdmin(), $this->getId()];
        if (empty($this->getPassword())) {
            unset($userData[2]);
        }

        if (!empty($this->getPassword())) {
            $passwordQuery = ', password=? ';
        }

        if (isset($_SESSION['account']) && $_SESSION['account']->getIsAdmin() != 1) {
            unset($userData[3]);
            $activeQuery = '';
            unset($userData[4]);
            $adminQuery = '';
        }

        try {
            $userData = array_values($userData);
            $userQuery = 'UPDATE users SET email=?, username=? ' . $passwordQuery . $activeQuery . $adminQuery .' WHERE id=?';
            $database = new Database();
            $database->ExecuteStoreQuery($userQuery, $userData);

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Setting up model
     *
     * @param int $id
     * @return Account
     */
    public function get(int $id): Account
    {
        $database = new Database();
        $query = 'SELECT * FROM users WHERE id = ?';
        $executable = [$id];
        $data = $database->ExecuteGetQuery($query, $executable);

        $this->setId($data[0]['id']);
        $this->setEmail($data[0]['email']);
        $this->setUsername($data[0]['username']);
        $this->setPassword($data[0]['password']);
        $this->setPasswordRepeat($data[0]['password']);
        $this->setActive($data[0]['active']);
        $this->setIsAdmin($data[0]['admin']);
        $this->setMap();

        return $this;
    }

    /**
     * On account update for current user, reinitialize session data
     */
    public function ResetSession()
    {
        if (isset($_SESSION['account'])) {
            $accountId = $_SESSION['account']->getId();
            if ($accountId == $this->getId()) {
                $_SESSION['account'] = $this;
            }
        }
    }

    /**
     * @return mixed
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @param $profile
     */
    public function setProfile($profile): void
    {
        $this->profile = $profile;
    }

    /**
     * @return mixed
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * @param mixed $operation
     */
    public function setOperation($operation): void
    {
        $this->operation = $operation;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getPasswordRepeat()
    {
        return $this->passwordRepeat;
    }

    /**
     * @param mixed $passwordRepeat
     */
    public function setPasswordRepeat($passwordRepeat): void
    {
        $this->passwordRepeat = $passwordRepeat;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active): void
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIsAdmin()
    {
        return $this->isAdmin;
    }

    /**
     * @param mixed $isAdmin
     */
    public function setIsAdmin($isAdmin): void
    {
        $this->isAdmin = $isAdmin;
    }

    /**
     * @return array
     */
    public function getMap(): array
    {
        return $this->map;
    }

    public function setMap(): void
    {
        $this->mapData();
    }

    /**
     * Map request data field with model elements
     */
    protected function mapData(): void
    {
        $this->map = [
            'account_id'      => $this->getId(),
            'id'              => $this->getId(),
            'username'        => $this->getUsername(),
            'email'           => $this->getEmail(),
            'active'          => $this->getActive(),
            'isAdmin'         => $this->getIsAdmin(),
            'password'        => $this->getPassword(),
            'password_repeat' => $this->getPasswordRepeat(),
            'profile'         => $this->getProfile(),
        ];
    }
}