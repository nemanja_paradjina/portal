<?php

namespace Modules\Account\Model;

use Modules\Account\Abstraction\ModelDefinition;
use Modules\Account\Controllers\Validators\ProfileValidator;
use Modules\Db\Database;

class Profile extends ModelDefinition
{
    public $required = ['first_name', 'last_name'];
    private $firstName;
    private $lastName;
    private $description;
    private $originalImage;
    private $resizedImage;
    private $publicProfile;
    private $accountId;
    private $map;
    private $profileValidator;

    public function __construct($request, $operation = '')
    {
        $this->setFirstName($request['first_name']);
        $this->setLastName($request['last_name']);
        $this->setDescription($request['description']);
        $this->setOriginalImage($request['image_source'] ?? '');
        $this->setResizedImage($request['image_resized'] ?? '');
        $this->setPublicProfile($request['public'] ?? 0);
        $this->setAccountId($request['account_id'] ?? null);

        $this->mapData();

        if (!empty($operation)) {
            $this->profileValidator = new ProfileValidator($this);
        }
    }

    /**
     * Update profile data
     *
     * @return bool
     */
    public function Update()
    {
        $imageQueryExtension = ', image_source=?, image_resized=?';
        $imageRemoved = $this->getOriginalImage() === null && $this->getResizedImage() === null;
        if ($imageRemoved) {
            $this->setOriginalImage('');
            $this->setResizedImage('');
        }

        try {
            $profileData = [$this->getFirstName(), $this->getLastName(), $this->getDescription(), $this->getOriginalImage(), $this->getResizedImage(), $this->getPublicProfile(), $this->getAccountId()];

            if (!$imageRemoved) {
                if(empty($this->getOriginalImage()) || empty($this->getResizedImage())) {
                    $imageQueryExtension = '';
                    unset($profileData[3]);
                    unset($profileData[4]);
                    $profileData = array_values($profileData);
                }
            }

            $profileQuery = 'UPDATE profile SET first_name=?, last_name=?, description=?' . $imageQueryExtension . ', public=? WHERE account_id=?';
            $database = new Database();
            $database->ExecuteStoreQuery($profileQuery, $profileData);

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Creating profile
     *
     * @return bool|mixed
     */
    public function Create()
    {
        try {
            $profileQuery = 'INSERT INTO profile(first_name, last_name, description, image_source, image_resized, public, account_id) VALUES(?, ?, ?, ?, ?, ?, ?)';
            $profileData = [$this->getFirstName(), $this->getLastName(), $this->getDescription(), $this->getOriginalImage(), $this->getResizedImage(), $this->getPublicProfile(), $this->getAccountId()];
            $database = new Database();

            return $database->ExecuteStoreQuery($profileQuery, $profileData);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Returns information if account data is valid
     *
     * @return bool
     */
    public function Valid(): bool
    {
        return $this->profileValidator->Valid();
    }

    /**
     * Pass messages from validator
     *
     * @return array
     */
    public function Messages(): array
    {
        return $this->profileValidator->getMessages();
    }

    /**
     * Returns profile data map
     *
     * @return array
     */
    public function getMap(): array
    {
        return $this->map;
    }

    /**
     * Setter for data mapping
     */
    public function setMap(): void
    {
        $this->mapData();
    }

    /**
     * Map request data field with model elements
     */
    protected function mapData(): void
    {
        $this->map = [
            'first_name'    => $this->getFirstName(),
            'last_name'     => $this->getLastName(),
            'description'   => $this->getDescription(),
            'image_source'  => $this->getOriginalImage(),
            'image_resized' => $this->getResizedImage(),
            'public'        => $this->getPublicProfile(),
            'account_id'    => $this->getAccountId()
        ];
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getOriginalImage()
    {
        return $this->originalImage;
    }

    /**
     * @param mixed $originalImage
     */
    public function setOriginalImage($originalImage): void
    {
        $this->originalImage = $originalImage;
    }

    /**
     * @return mixed
     */
    public function getResizedImage()
    {
        return $this->resizedImage;
    }

    /**
     * @param mixed $resizedImage
     */
    public function setResizedImage($resizedImage): void
    {
        $this->resizedImage = $resizedImage;
    }

    /**
     * @return mixed
     */
    public function getPublicProfile()
    {
        return $this->publicProfile;
    }

    /**
     * @param mixed $publicProfile
     */
    public function setPublicProfile($publicProfile): void
    {
        $this->publicProfile = $publicProfile;
    }

    /**
     * @return mixed
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * @param mixed $accountId
     */
    public function setAccountId($accountId): void
    {
        $this->accountId = $accountId;
    }
}