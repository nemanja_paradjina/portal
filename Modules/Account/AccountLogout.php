<?php


namespace Modules\Account;

/**
 * Class AccountLogin
 *
 * @package Modules\Account
 */
class AccountLogout
{
    private const LOGOUT_SUCCESSFUL = 'Logout Successful.';

    private $messages;

    /**
     * Defining logout page
     *
     * @param array $request
     * @return array
     */
    public function PageLogout(array $request): array
    {
        unset($_SESSION['account']);
        $this->messages['info'][] = self::LOGOUT_SUCCESSFUL;
        $template = __DIR__ . '/Templates/Logout.php';
        $title = self::LOGOUT_SUCCESSFUL;
        $pageTitle = self::LOGOUT_SUCCESSFUL;

        return [
            'template' => $template,
            'request' => [],
            'data'    => [
                'title' => $title,
                'page_title' => $pageTitle,
                'messages' => $this->messages
            ]
        ];
    }
}