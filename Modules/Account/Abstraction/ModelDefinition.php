<?php

namespace Modules\Account\Abstraction;

/**
 * Class ModelDefinition
 * @package Modules\Account\Abstraction
 */
abstract class ModelDefinition
{
    abstract public function getMap(): array;
    abstract protected function mapData(): void;
}