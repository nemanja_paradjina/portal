<?php

namespace Modules;

use Modules\Account\Controllers\AccountController;
use Modules\Account\Controllers\ProfileController;
use Modules\Account\Model\Account as AccountModel;

/**
 * Class Account
 *
 * @package Modules\Account
 */
class Account
{
    private $messages = [];

    private $accountController;
    private $profileController;

    public function __construct()
    {
        $this->accountController = new AccountController();
        $this->profileController = new ProfileController();
    }

    /**
     * Preview profile page based on account ID
     *
     * @param array $request
     * @return array
     */
    public function PageProfile(array $request): array
    {
        $accountModel = new AccountModel();
        $account = $accountModel->get($request['account']);
        $profile = $this->profileController->getOne($account->getId(), 'account_id');
        $fields = array_merge($account->getMap(), $profile->getMap());
        $template = __DIR__ . '/Templates/Account.php';
        $fullName = $profile->getFirstName() . ' ' . $profile->getLastName();
        $title = getTitle($fullName);

        if (isset($_SESSION['messages'])) {
            $this->messages = array_merge($_SESSION['messages'], $this->messages);
            unset($_SESSION['messages']);
        }

        return [
            'template' => $template,
            'request' => $request,
            'data'    => [
                'title' => $title,
                'page_title' => $fullName,
                'fields' => $fields,
                'messages' => $this->messages
            ]
        ];
    }


}
