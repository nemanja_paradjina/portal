<?php


namespace Modules\Account;

use Modules\Account\Controllers\AccountController;
use Modules\Account\Controllers\ProfileController;
use Modules\Account\Model\Account as AccountModel;
use Modules\Token;

/**
 * Class AccountRegistration
 * @package Modules\Account
 */
class AccountRegistration
{
    private const CREATE_ACCOUNT_PAGE_TITLE = 'Create Account';
    private const ACCOUNT_CREATE_NOT_ALLOWED = 'You don\'t have permission to create account';

    private const ACCOUNT_CREATED_ACTIVATION_REQUIRED_TITLE = 'Account created.';
    private const ACCOUNT_CREATED_ACTIVATION_REQUIRED = 'Your account has been created but it is not active. To activate your account please click on activation link in you mailbox.';

    private const ACCOUNT_ACTIVATED_TITLE = 'Account activated.';
    private const ACCOUNT_ACTIVATED_MESSAGE = 'Account activated. You may now <a href="/account/login">login</a> to continue.';

    private const TOKEN_EXPIRED_TITLE = 'Token expired.';
    private const TOKEN_EXPIRED = 'Token expired. You most likely clicked on the link in your mailbox.';

    private $accountController;
    private $profileController;
    private $messages = [];

    public function __construct()
    {
        $this->accountController = new AccountController();
        $this->profileController = new ProfileController();
    }

    /**
     * Providing registration form for non registered users
     *
     * @param array $request
     * @return array
     */
    public function PageAccountRegister(array $request)
    {
        $template = __DIR__ . '/Templates/EditAccount.php';
        $title = getTitle(self::CREATE_ACCOUNT_PAGE_TITLE);
        $pageTitle = self::CREATE_ACCOUNT_PAGE_TITLE;

        return [
            'template' => $template,
            'request' => $request,
            'data'    => [
                'title' => $title,
                'page_title' => $pageTitle,
                'fields' => [],
                'messages' => $this->messages,
                'action_type' => 'create',
            ]
        ];
    }

    /**
     * Registration form submit
     *
     * @param array $request
     * @return array
     */
    public function StoreAccountRegistration(array $request)
    {
        $this->accountController->CheckPermissions('create_account', null, self::ACCOUNT_CREATE_NOT_ALLOWED);
        $data = $this->accountController->storeAccount($request, true);

        $template = __DIR__ . '/Templates/EditAccount.php';
        $title = getTitle(self::CREATE_ACCOUNT_PAGE_TITLE);
        $pageTitle = self::CREATE_ACCOUNT_PAGE_TITLE;
        $messages = array_merge($this->messages, $data['messages']);

        return [
            'template' => $template,
            'request' => $request,
            'data'    => [
                'title' => $title,
                'page_title' => $pageTitle,
                'fields' => $data['fields'] ?? [],
                'messages' => $messages,
                'action_type' => 'create',
            ]
        ];
    }

    /**
     * Activation required info page, if user registered successfully.
     *
     * @param array $request
     * @return array
     */
    public function PageAccountActivationRequired(array $request)
    {
        $template = '';
        $title = getTitle(self::ACCOUNT_CREATED_ACTIVATION_REQUIRED_TITLE);
        $pageTitle = self::ACCOUNT_CREATED_ACTIVATION_REQUIRED_TITLE;
        $this->messages['success'][] = self::ACCOUNT_CREATED_ACTIVATION_REQUIRED;

        return [
            'template' => $template,
            'request' => $request,
            'data'    => [
                'title' => $title,
                'page_title' => $pageTitle,
                'messages' => $this->messages,
                'action_type' => 'create',
            ]
        ];
    }

    /**
     * Account activation
     *
     * @param array $request
     * @return array
     */
    public function PageAccountActivate(array $request)
    {
        if (!empty($request['activate'])) {
            $token = new Token();
            $tokenData = $token->getByHash($request['activate']);
            if (null != $tokenData) {
                $accountId = $tokenData->getAccountId();
                $account = new AccountModel();
                $account = $account->get($accountId);
                $account->setActive(1);
                $account->setPassword('');
                $account->setPasswordRepeat('');
                $account->setMap();
                $account->Update();
                $token->remove($tokenData->getId());
                $title = getTitle(self::ACCOUNT_ACTIVATED_TITLE);
                $pageTitle = self::ACCOUNT_ACTIVATED_TITLE;
                $this->messages['success'][] = self::ACCOUNT_ACTIVATED_MESSAGE;
            } else {
                $title = getTitle(self::TOKEN_EXPIRED_TITLE);
                $pageTitle = self::TOKEN_EXPIRED_TITLE;
                $this->messages['alert'][] = self::TOKEN_EXPIRED;
            }

            return [
                'template' => '',
                'request' => $request,
                'data'    => [
                    'title' => $title,
                    'page_title' => $pageTitle,
                    'messages' => $this->messages,
                    'action_type' => 'create',
                ]
            ];
        }

        redirect('/');
        return [];
    }
}