<?php

namespace Modules\Account\Controllers;

use Modules\Account\Model\Profile;
use Modules\Db\Database;
use Modules\FileManager;

class ProfileController
{
    private $database;

    use FileManager\FileHandlerTrait;

    public function __construct()
    {
        $this->database = new Database();
    }

    /**
     * Get account data based on id
     *
     * @param int $id
     * @param string $column
     * @return Profile
     */
    public function getOne(int $id, string $column = '')
    {
        $where = ' WHERE id = ? ';
        if (!empty($column)) {
            $where = ' WHERE ' . $column . ' = ? ';
        }
        $query = 'SELECT * FROM profile ' . $where;
        $queryParams = [$id];
        $data = $this->database->ExecuteGetQuery($query, $queryParams);
        return new Profile($data[0]);
    }

    /**
     * Get all profiles or all public
     *
     * @param array $status
     * @return array
     */
    public function getAll(array $status = ['public' => false]): array
    {
        $profiles = [];
        $queryExtend = '';

        if (true === $status['public']) {
            $queryExtend = 'WHERE public=1';
        }

        $query = 'SELECT * FROM profile  ' . $queryExtend;
        $data = $this->database->ExecuteGetQuery($query, []);

        foreach ($data as $profile) {
            $profiles[] = new Profile($profile);
        }

        return $profiles;
    }
}