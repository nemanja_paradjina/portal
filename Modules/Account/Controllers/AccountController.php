<?php

namespace Modules\Account\Controllers;

use Modules\Account\Controllers\Validators\AccountValidator;
use Modules\Account\Model\Account;
use Modules\Account\Model\Profile;
use Modules\Db\Database;
use Modules\EmailEngine;
use Modules\PermissionManager;
use Modules\Token;

class AccountController
{
    private $database;

    private const USER_INVALID = 'Invalid credentials.';
    private const USER_INACTIVE = 'Account is not active.';

    private const EMAIL_ACCOUNT_CREATED_SUBJECT = 'PORTAL: Account created';
    private const EMAIL_ACCOUNT_CREATED_BODY = 'Thank you %s for registering to our portal! <br />Your account has been created. In order to activate your account, please click on the following <a href="%s/account/activate/%s">link</a>';

    public function __construct()
    {
        $this->database = new Database();
    }

    /**
     * Get all accounts
     *
     * @param bool $includeProfile
     * @return array
     */
    public function getAll(bool $includeProfile = false)
    {
        $query = 'SELECT * FROM users';
        $data = $this->database->ExecuteGetQuery($query, []);
        $list = [];
        foreach ($data as $account) {
            $profileController = new ProfileController();
            $profile = $profileController->getOne($account['id'], 'account_id');
            $accountModel = new Account();
            $account = $accountModel->get($account['id']);
            $account->setProfile($profile);
            $list[] = $account;
        }

        return $list;
    }

    /**
     * Validating user and initiating session if user exists
     *
     * @param array $request
     * @return array
     */
    public function isValidUser(array $request)
    {
        $account = $this->getByCredentials($request);

        $messages = [];

        if ($account == false) {
            $messages['alert'][] = self::USER_INVALID;
            return $messages;
        }

        if (0 == $account->getActive()) {
            $messages['alert'][] = self::USER_INACTIVE;
            return $messages;
        }

        $_SESSION['account'] = $account;
    }

    /**
     * Get account by email
     *
     * @param array $request
     * @return bool|Account
     */
    public function getByEmail(array $request): ?Account
    {
        $query = 'SELECT id FROM users WHERE email = ?';
        $executable = [$request['email']];
        $data = $this->database->ExecuteGetQuery($query, $executable);
        if (!empty($data)) {
            $account = new Account();
            return $account->get($data[0]['id']);
        }

        return null;
    }

    /**
     * Get account by login credentials
     *
     * @param array $request
     * @return bool|Account
     */
    public function getByCredentials(array $request): ?Account
    {
        $query = 'SELECT id FROM users WHERE username = ? AND password = ?';
        $executable = [$request['username'], md5($request['password'])];
        $data = $this->database->ExecuteGetQuery($query, $executable);

        if (!empty($data)) {
            $account = new Account();
            return $account->get($data[0]['id']);
        }

        return null;
    }

    /**
     * Checking if has permission to access the page
     *
     * @param string $permission
     * @param $data
     * @param string $message
     */
    public function CheckPermissions(string $permission, $data = null, string $message)
    {
        $permissionManager = new PermissionManager();
        if (!$permissionManager->can($permission, $data)) {
            $_SESSION['messages']['alert'][] = $message;
            redirect('/error');
            die();
        }
    }

    /**
     * Common method for storing account for user registration and administration account creation
     *
     * @param array $request
     * @param bool $anonymous
     * @return array
     */
    public function storeAccount(array $request, $anonymous = false)
    {
        $profileController = new ProfileController();
        $action = 'create';
        $account = $this->prepareStoreAccountData($request);
        $accountValidator = new AccountValidator($account);
        $profile = new Profile($request, $action);
        $dataIsValid = $accountValidator->Valid() && $profile->Valid();
        $files = [];
        if ($dataIsValid == true) {
            $files = $profileController->HandleFiles($request);
            if (!empty($files)) {
                $profile->setOriginalImage($files['image_source']);
                $profile->setResizedImage($files['image_resized']);
            }
        }

        if ($dataIsValid == true && $files !== NULL) {
            $accountId = $account->Create();
            $account->setId($accountId);
            $profile->setAccountId($accountId);
            $profileCreated = $profile->Create();

            if ($accountId !== false && $profileCreated !== false) {
                if ($anonymous == false) {
                    redirect('/account/' . $accountId);
                    return [];
                }
                $token = new Token();
                $token = $token->generateToken($account, 'AccountRegistration')->get();
                $body = sprintf(self::EMAIL_ACCOUNT_CREATED_BODY, $account->getUsername(), SITE_URL, $token);
                $email = new EmailEngine($account->getEmail(), SITE_EMAIL, self::EMAIL_ACCOUNT_CREATED_SUBJECT, $body);
                $email->send();
                redirect('/account/activation-required');
                return [];
            }
        }
        $messages = array_merge($accountValidator->getMessages(), $profile->Messages());
        $fields = array_merge($account->getMap(), $profile->getMap());

        return [
            'messages' => $messages,
            'fields'   => $fields,
        ];
    }

    /**
     * Setting data for insert
     *
     * @param $request
     * @return Account
     */
    public function prepareStoreAccountData($request): Account
    {
        $account = new Account();
        if (isset($request['account_id'])) {
            $account->setId($request['account_id']);
        }

        if (isset($request['id'])) {
            $account->setId($request['id']);
        }

        $account->setOperation('create');
        $account->setUsername($request['username']);
        $account->setEmail($request['email']);
        $account->setActive($request['active'] ?? 0);
        $account->setIsAdmin($request['admin'] ?? 0);
        $account->setIsAdmin($request['admin'] ?? 0);
        $request['password'] = !empty($request['password']) ? md5($request['password']) : '';
        $request['password_repeat'] = !empty($request['password_repeat']) ? md5($request['password_repeat']) : '';
        $account->setPassword($request['password'] ?? '');
        $account->setPasswordRepeat($request['password_repeat'] ?? '');
        $account->setMap();
        return $account;
    }
}