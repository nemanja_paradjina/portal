<?php

namespace Modules\Account\Controllers\Validators;

use Modules\Account\Model\Account;
use Modules\Db\Database;
/**
 * Class AccountValidator
 * @package Modules\Account\Controllers\Validators
 */
class AccountValidator
{
    private $required;
    private $data;
    private $messages = [];
    private $account;
    private $noPass = false;
    protected $database;

    public function __construct(Account $account)
    {
        $this->account = $account;
        $this->required = $account->required[$account->getOperation()];
        $this->data = $account->getMap();

        $this->database = new Database();

        $this->ValidateRequired();
        $this->ValidateEquality();
        $this->ValidateIfUnique();
    }

    /**
     * Returns information if model is valid
     *
     * @return bool
     */
    public function Valid(): bool
    {
        return !$this->noPass;
    }

    /**
     * Gets array of generated messages
     *
     * @return mixed
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

    /**
     * Checks if required fields are empty
     */
    private function ValidateRequired(): void
    {
        foreach ($this->data as $key => $field) {
            if (in_array($key, $this->required) && empty($field)) {
                $this->noPass = true;
                $this->messages['alert'][] = 'Field ' . $key . ' cannot be empty.';
            }
        }
    }

    /**
     * Validating if password and password repeat are equal
     */
    private function ValidateEquality(): void
    {
        if ($this->data['password'] != $this->data['password_repeat']) {
            $this->noPass = true;
            $this->messages['alert'][] = 'Fields "Password" and "Repeat Password" must be the same.';
        }
    }

    /**
     * We shouldn't allow user to use someone's email or username
     */
    private function ValidateIfUnique(): void
    {
        $queryExtend = '';
        $parameters = [$this->data['username'], $this->data['email']];

        if ($this->account->getOperation() == 'update') {
            $queryExtend = 'AND id != ?';
            array_push($parameters, $this->data['account_id']);
        }

        $query = 'SELECT * FROM users WHERE (username=? OR email=?) ' . $queryExtend;
        $users = $this->database->ExecuteGetQuery($query, $parameters);

        if (0 != count($users)) {
            $this->noPass = true;
            $this->messages['alert'][] = 'Username or email are already used, try different data.';
        }
    }
}