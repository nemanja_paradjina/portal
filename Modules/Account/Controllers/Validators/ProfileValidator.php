<?php

namespace Modules\Account\Controllers\Validators;

use Modules\Account\Model\Profile;
use Modules\Db\Database;
/**
 * Class AccountValidator
 * @package Modules\Account\Controllers\Validators
 */
class ProfileValidator
{
    private $data;
    private $messages = [];
    private $profile;
    private $noPass = false;
    protected $database;

    public function __construct(Profile $profile)
    {
        $this->profile = $profile;
        $this->data = $profile->getMap();
        $this->database = new Database();

        $this->ValidateRequired();
    }

    /**
     * Returns information if model is valid
     *
     * @return bool
     */
    public function Valid()
    {
        return !$this->noPass;
    }

    /**
     * Gets array of generated messages
     *
     * @return mixed
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

    /**
     * Checks if required fields are empty
     */
    private function ValidateRequired(): void
    {
        foreach ($this->data as $key => $field) {
            if (in_array($key, $this->profile->required) && empty($field)) {
                $this->noPass = true;
                $this->messages['alert'][] = 'Field ' . $key . ' cannot be empty.';
            }
        }
    }
}