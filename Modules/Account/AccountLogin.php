<?php

namespace Modules\Account;

use Modules\Account\Controllers\AccountController;
use Modules\Account\Controllers\ProfileController;
use Modules\PermissionManager;

/**
 * Class AccountLogin
 *
 * @package Modules\Account
 */
class AccountLogin
{
    private const LOGIN_TO_CONTINUE = 'Please login to continue';
    private const PAGE_TITLE = 'Please login to continue';
    private const LOGIN_SUCCESSFUL = 'Login Successful.';

    private $messages;
    private $accountController;
    private $profileController;

    public function __construct()
    {
        $this->accountController = new AccountController();
        $this->profileController = new ProfileController();
    }

    /**
     * Providing access based on sent request
     *
     * @param array $request
     * @return array
     */
    public function PageLogin(array $request): ?array
    {
        $template = __DIR__ . '/Templates/Login.php';
        $title = getTitle(self::LOGIN_TO_CONTINUE);
        $pageTitle = self::PAGE_TITLE;

        if (isset($request['login'])) {
            $this->messages = $this->accountController->isValidUser($request);
            if (isset($_SESSION['account'])) {
                $_SESSION['messages']['success'][] = self::LOGIN_SUCCESSFUL;

                $permissionManager = new PermissionManager();
                if ($permissionManager->can('list_profiles')) {
                    redirect('/account/list');
                    return [];
                }
                redirect('/account/' . $_SESSION['account']->getId());
                return [];
            }
        }

        return [
            'template' => $template,
            'request' => $request,
            'data'    => [
                'title' => $title,
                'page_title' => $pageTitle,
                'fields' => $fields ?? [],
                'messages' => $this->messages
            ]
        ];
    }
}