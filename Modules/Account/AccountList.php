<?php

namespace Modules\Account;

use Modules\Account\Controllers\AccountController;
use Modules\Account\Controllers\ProfileController;

/**
 * Class AccountList
 *
 * @package Modules\Account
 */
class AccountList
{
    private const ACCOUNT_MANAGE_NOT_ALLOWED = 'You don\'t have permission to manage accounts';
    private const ACCOUNTS_PAGE_TITLE = 'Profile list';

    private $accountController;
    private $messages = [];

    public function __construct()
    {
        $this->accountController = new AccountController();
    }

    /**
     * List all profiles
     *
     * @param array $request
     * @return array
     */
    public function PageAccountList(array $request): array
    {
        $this->accountController->CheckPermissions('list_profiles', null, self::ACCOUNT_MANAGE_NOT_ALLOWED);
        $template = __DIR__ . '/Templates/Accounts.php';
        $title = getTitle(self::ACCOUNTS_PAGE_TITLE);
        $pageTitle = self::ACCOUNTS_PAGE_TITLE;
        $accountList = $this->accountController->getAll(true);

        if (isset($_SESSION['messages'])) {
            $this->messages = array_merge($_SESSION['messages'], $this->messages);
            unset($_SESSION['messages']);
        }

        return [
            'template' => $template,
            'request' => $request,
            'data'    => [
                'title' => $title,
                'page_title' => $pageTitle,
                'accountList' => $accountList ?? [],
                'messages' => $this->messages
            ]
        ];
    }
}