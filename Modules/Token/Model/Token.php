<?php


namespace Modules\Token\Model;

use Modules\Db\Database;

/**
 * Class Token
 * @package Modules\Token\Model
 */
class Token
{
    private $id;
    private $hash;
    private $type;
    private $account_id;

    private $database;

    public function __construct()
    {
        $this->database = new Database();
    }

    /**
     * Token create method
     *
     * @return mixed
     */
    public function create()
    {
        $query = 'INSERT INTO token(hash, type, account_id) VALUES(?,?,?)';
        $executable = [$this->getHash(), $this->getType(), $this->getAccountId()];
        return $this->database->ExecuteStoreQuery($query, $executable);
    }

    /**
     * Token delete method
     *
     * @param int $id
     * @param string $column
     * @param string $type
     * @return mixed
     */
    public function delete(int $id, string $column = '', string $type = '')
    {
        $queryString = '';
        $params = [$id];
        switch ($column) {
            case 'account_id':
                $queryString = ' WHERE account_id=?';
            break;
            default:
                $queryString = ' WHERE id=?';
            break;
        }

        switch ($type) {
            case 'passwordReset':
                $queryString .= ' AND type=? ';
                $params[] = $type;
            break;
        }

        $query = 'DELETE FROM token ' . $queryString;
        return $this->database->ExecuteDeleteQuery($query, $params);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     */
    public function setHash($hash): void
    {
        $this->hash = $hash;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getAccountId()
    {
        return $this->account_id;
    }

    /**
     * @param mixed $account_id
     */
    public function setAccountId($account_id): void
    {
        $this->account_id = $account_id;
    }


}