<?php

namespace Modules;

use Modules\Db\Database;
use Modules\Account\Model\Account;
use Modules\Token\Model\Token as TokenModel;

class Token
{
    private const SECRET_KEY = 'aj289@&#)(j.88';

    private $database;
    private $token;

    public function __construct()
    {
        $this->database = new Database();
    }

    /**
     * Method for generating token (type: forgot password or create account)
     *
     * @param Account $account
     * @param string $type
     * @return Token
     */
    public function generateToken(Account $account, string $type): Token
    {
        $tokenString = md5(time() . $account->getEmail() . self::SECRET_KEY);
        $token = new TokenModel();

        if ('passwordReset' == $type) {
            $token->delete($account->getId(), 'account_id', $type);
        } else {
            $token->delete($account->getId());
        }

        $token->setAccountId($account->getId());
        $token->setHash($tokenString);
        $token->setType($type);
        $token->create();
        $this->token = $token;

        return $this;
    }

    /**
     * When token generated successfully it can be retrieved with get method
     */
    public function get()
    {
        return $this->token->getHash();
    }

    /**
     * Get token by provided hash
     *
     * @param string $hash
     * @return TokenModel
     */
    public function getByHash(string $hash): ?TokenModel
    {
        $query = 'SELECT * FROM token WHERE hash=?';
        $queryData = [$hash];
        $tokens = $this->database->ExecuteGetQuery($query, $queryData);
        if (count($tokens) > 0) {
            $token = new TokenModel();
            $token->setId($tokens[0]['id']);
            $token->setType($tokens[0]['type']);
            $token->setHash($tokens[0]['hash']);
            $token->setAccountId($tokens[0]['account_id']);
            return $token;
        }

        return null;
    }

    /**
     * When user uses token it should be removed
     *
     * @param int $id
     * @return mixed
     */
    public function remove(int $id)
    {
        $tokenModel = new TokenModel();
        return $tokenModel->delete($id);
    }
}