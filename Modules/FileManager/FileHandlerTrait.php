<?php

namespace Modules\FileManager;

use Modules\FileManager;

trait FileHandlerTrait {

    /**
     * Removing image
     *
     * @param array $request
     * @return Profile
     */
    public function removeImages($entity)
    {
        $originalImage = $entity->getOriginalImage();
        $resizedImage = $entity->getResizedImage();

        $originalImagePath = $_SERVER['DOCUMENT_ROOT'] . $originalImage;
        $resizedImagePath  = $_SERVER['DOCUMENT_ROOT'] . $resizedImage;

        if (!empty($originalImage) && file_exists($originalImagePath)) {
            unlink($originalImagePath);
        }
        if (!empty($resizedImage) && file_exists($resizedImagePath)) {
            unlink($resizedImagePath);
        }
        $entity->setOriginalImage(null);
        $entity->setResizedImage(null);
        $entity->setMap();

        return $entity;
    }

    /**
     * Images upload
     *
     * @param array $request
     * @return array
     */
    public function HandleFiles(array $request): ?array
    {
        $files = [];

        if (isset($request['files']) && !empty($request['files']['image_source']['tmp_name'])) {
            $fileManager = new FileManager($request['files']);
            $images = $fileManager->storeImages('image_source');

            if (empty($images)) {
                $this->messages['alert'] = $fileManager->messages['alert'];
                return null;
            }

            if (count($images) > 0) {
                $files['image_source'] = $images['original'][0];
                $files['image_resized'] = $images['resized'][0];
            }
        }
        return $files;
    }
}
