<?php

namespace Modules;

use Imagick;

/**
 * Class FileManager
 *
 * @package Modules
 */
class FileManager
{
    private $files;
    private $allowedImageTypes = [
        'image_source' => ['image/jpeg', 'image/png'],
    ];
    public $messages = [];

    public function __construct(array $files)
    {
        $this->files = $files;
    }

    /**
     * Main function for storing images
     *
     * @param string $field
     * @return array
     */
    public function storeImages(string $field): array
    {
        $path = $_SERVER['DOCUMENT_ROOT'] . '/Images/' . $field . '/';
        $names = [];
        foreach ($this->files as $file) {
            $name = $file['name'];
            if (!in_array($file['type'], $this->allowedImageTypes[$field])) {
                $this->messages['alert'][] = 'Unsupported file type';
                return [];
            }

            if ($this->isUnique($file, $field)) {
                $moved = move_uploaded_file($file['tmp_name'], $path . $name);
                if ($moved) {
                    $names[] = $name;
                }
                continue;
            }

            $name = $this->generateUniqueName($file['name'], $path);
            $moved = move_uploaded_file($file['tmp_name'], $path . $name);
            if ($moved) {
                $names[] = $name;
            }
            continue;
        }

        $images['original'] = $this->getPublicPaths($names, $field, 'images');
        $images['resized']  = $this->getResizedImages($images['original']);
        return $images;
    }

    /**
     * Added black canvas to resized images
     *
     * @param $images
     * @return array
     * @throws \ImagickException
     */
    private function getResizedImages($images)
    {
        $resizedImages = [];
        $canvas = new Imagick();
        $finalWidth = 150;
        $finalHeight = 150;
        $canvas->newImage($finalWidth, $finalHeight, 'black');

        foreach ($images as $image) {
            $thumb = new Imagick();
            $thumbAspectSize = $this->getThumbnailAspectRatio($_SERVER['DOCUMENT_ROOT'] . $image, 150, 150);
            $thumb->readImage($_SERVER['DOCUMENT_ROOT'] . $image);
            $thumb->scaleImage($thumbAspectSize['thumbWidth'], $thumbAspectSize['thumbHeight']);
            $offsetX = (int)($finalWidth  / 2) - (int)($thumbAspectSize['thumbWidth']  / 2);
            $offsetY = (int)($finalHeight / 2) - (int)($thumbAspectSize['thumbHeight'] / 2);
            $canvas->compositeImage( $thumb, imagick::COMPOSITE_OVER, $offsetX, $offsetY );

            $imageSeparation = explode('.', $image);
            $resizedImageName = $imageSeparation[0] . '_resized' . '.' . $imageSeparation[1];
            $canvas->writeImage($_SERVER['DOCUMENT_ROOT'] . $resizedImageName);
            $resizedImages[] = $resizedImageName;
            $canvas->clear();
            $canvas->destroy();
        }

        return $resizedImages;
    }

    /**
     * Getting the size of the image by keeping the aspect ratio
     *
     * @param string $originalFile
     * @param $width
     * @param $height
     * @return array
     */
    private function getThumbnailAspectRatio(string $originalFile, $width, $height)
    {
        list($w, $h) = getimagesize($originalFile);
        if($w > $width OR $h > $height){
            if($w >= $h){
                $thumbW = $width;
                $thumbRatio = $w / $width;
                $thumbH = (int)($h / $thumbRatio);
            } else {
                $thumbH = $height;
                $thumbRatio = $h / $height;
                $thumbW = (int)($w / $thumbRatio);
            }
        } else {
            $thumbW = $w;
            $thumbH = $h;
        }
        return [
            'thumbWidth' => $thumbW,
            'thumbHeight' => $thumbH
        ];
    }

    /**
     * Preparing file paths for storage
     *
     * @param array $fileNames
     * @param string $field
     * @param string $type
     * @return array
     */
    private function getPublicPaths(array $fileNames, string $field, string $type)
    {
        $fileCategory = '';
        $filenames = [];

        switch ($type) {
            case 'images':
                $fileCategory = 'Images';
            break;
        }

        foreach ($fileNames as $f => $fileName) {
            $filenames[$f] = '/' . $fileCategory . '/' . $field . '/' . $fileName;
        }

        return $filenames;
    }

    /**
     * Adds extension _2, _3 to filenames if they already exist
     *
     * @param string $name
     * @param string $path
     * @return string
     */
    private function generateUniqueName(string $name, string $path): string
    {
        $fileNumber = 0;

        while (file_exists($path . $name)) {
            $extensionSeparation = explode('.', $name);
            $nameSeparation = explode('_', $extensionSeparation[0]);
            if (is_numeric($nameSeparation[count($nameSeparation) - 1]) && $nameSeparation[count($nameSeparation) - 1] != $nameSeparation[0]) {
                $fileNumber = $nameSeparation[count($nameSeparation) - 1];
                $fileNumber++;
                unset($nameSeparation[count($nameSeparation) - 1]);
            } else {
                $fileNumber++;
            }

            $name = implode('_', $nameSeparation) . '_' . $fileNumber . '.' . $extensionSeparation[1];
            if (!file_exists($path . $name)) {
                return $name;
            }
        }
    }

    /**
     * Checks if file is unique for the given field
     *
     * @param array $file
     * @param string $field
     * @return bool
     */
    private function isUnique(array $file, string $field): bool
    {
        return !file_exists($_SERVER['DOCUMENT_ROOT'] . '/Images/' . $field . '/' . $file['name']);
    }
}