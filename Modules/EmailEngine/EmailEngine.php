<?php

namespace Modules;

use PHPMailer\PHPMailer\PHPMailer;
/**
 * Class EmailEngine
 * @package Modules
 */
class EmailEngine
{
    private $mail;

    public function __construct(string $to, string $from, string $subject, string $body)
    {
        require_once $_SERVER['DOCUMENT_ROOT'] . '/../Libraries/PHPMailer/src/PHPMailer.php';
        require_once $_SERVER['DOCUMENT_ROOT'] . '/../Libraries/PHPMailer/src/Exception.php';
        require_once $_SERVER['DOCUMENT_ROOT'] . '/../Libraries/PHPMailer/src/SMTP.php';

        $mail = new PHPMailer(true);
        $mail->isSMTP();
        $mail->Host = SMTP_HOST;
        $mail->Port = SMTP_PORT;
        $mail->SMTPAuth = SMTP_AUTH;
        $mail->SMTPSecure = SMTP_SECURE;
        $mail->Username = SMTP_USERNAME ?? '';
        $mail->Password = SMTP_PASSWORD ?? '';

        $mail->addAddress($to);
        $mail->setFrom($from, 'Portal');
        $mail->addReplyTo($from);
        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $body;
        $this->mail = $mail;
    }

    public function send()
    {
        return $this->mail->send();
    }
}