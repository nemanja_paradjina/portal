<?php

use Modules\Db\Database;

/**
 * Trait CreateTrait
 */
trait CreateTrait
{
    public function create()
    {
        try {
            $map = $this->getMap();

            if ($this->getOperation() == 'create') {
                foreach ($map as $key => $value) {
                    if ($key === 'id') {
                        unset($map[$key]);
                    }
                }
            }

            $mapKeys = array_keys($map);
            $keys   = implode(',', $mapKeys);
            $values = array_values($map);

            $queryWildcharacters = '';
            foreach ($mapKeys as $key) {
                $queryWildcharacters .= '?,';
            }
            $queryWildcharacters = rtrim($queryWildcharacters, ',');

            $query = 'INSERT INTO ' . $this->table . '(' . $keys . ') VALUES(' . $queryWildcharacters . ')';
            $database = new Database();

            return $database->ExecuteStoreQuery($query, $values);
        } catch (Exception $e) {
            return false;
        }
    }
}