<?php

use Modules\Db\Database;
/**
 * Class UpdateTrait
 * @package Modules\Db\Traits
 */
trait UpdateTrait
{
    public function update()
    {
        try {
            $map = $this->getMap();
            $id = $map['id'];

            foreach ($map as $key => $value) {
                if ($key === 'id') {
                    unset($map[$key]);
                }
            }

            $updates = '';
            foreach ($map as $key => $mapItem) {
                $updates .= $key . '=?,';
            }
            $updates = rtrim($updates, ',');
            $values = array_values($map);
            $values[] = $id;

            $query = 'UPDATE ' . $this->table . ' SET ' . $updates . ' WHERE id = ?';
            $database = new Database();
            $database->ExecuteStoreQuery($query, $values);

            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}