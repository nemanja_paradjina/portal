<?php

namespace Modules\Db;

use \PDO;

/**
 * Class Database
 *
 * @package Modules\Db
 */
class Database
{
    public $connection;

    public function __construct()
    {
        $pdoSettings = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';';
        $options = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $this->connection = new PDO($pdoSettings, DB_USER, DB_PASSWORD, $options);
    }

    /**
     * Executing given query with provided params
     *
     * @param string $query
     * @param array $params
     * @return mixed
     */
    public function ExecuteGetQuery(string $query, array $params)
    {
        $queryPreparation = $this->connection->prepare($query);
        if (!$params) {
            $queryPreparation->execute();
            return $queryPreparation->fetchAll();
        }
        $queryPreparation->execute($params);
        return $queryPreparation->fetchAll();
    }

    /**
     * Executing given query with provided params
     *
     * @param string $query
     * @param array $params
     * @return mixed
     */
    public function ExecuteStoreQuery(string $query, array $params)
    {
        if (!$params) {
            throw new \LogicException('Missing parameters for executing query');
        }
        $queryPreparation = $this->connection->prepare($query);
        $queryPreparation->execute($params);
        return $this->connection->lastInsertId();
    }

    /**
     * Method for removal queries
     *
     * @param string $query
     * @param array $params
     * @return bool
     */
    public function ExecuteDeleteQuery(string $query, array $params = [])
    {
        $queryPreparation = $this->connection->prepare($query);
        if (!empty($params)) {
            return $queryPreparation->execute($params);
        }
    }
}