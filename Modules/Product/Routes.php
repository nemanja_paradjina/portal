<?php

global $routes;

$routes['/product/add'] = [
    'GET'  => 'Modules\Product|PageProductAdd',
    'POST' => 'Modules\Product|StoreProduct'
];

$routes['/products'] = [
    'GET' => 'Modules\Product|PageProducts',
];

$routes['/product/%'] = [
    'GET' => 'Modules\Product|PageProduct',
    'POST' => 'Modules\Product|StoreComment'
];