<?php

namespace Modules;

use Modules\Account\Controllers\AccountController;
use Modules\Comments\Controllers\CommentsController;
use Modules\Product\Controllers\ProductController;

/**
 * Class Product
 * @package Modules
 */
class Product
{
    public const ADDING_PRODUCT_NOT_ALLOWED = 'You are not allowed to add new product';
    public const WELCOME_TITLE = 'Welcome to our store!';
    public const ADD_PRODUCT_TITLE = 'Add new product';
    public const COMMENT_ADDED = 'Your comment has been submitted for review. After approval it will be visible on the page';

    private $accountController;

    public function __construct()
    {
        $this->accountController = new AccountController();
    }

    /**
     * Adding new comment
     *
     * @param array $request
     */
    public function StoreComment(array $request)
    {
        $commentController = new CommentsController();
        $comment = $commentController->createComment($request);
        if (null !== $comment) {
            $_SESSION['messages']['success'][] = self::COMMENT_ADDED;
            redirect('/product/' . $request['entity_id']);
        }
    }

    /**
     * Single product page
     *
     * @param array $request
     * @return array
     */
    public function PageProduct(array $request): array
    {
        $productController = new ProductController();
        $product = $productController->getById($request['product']);
        $commentController = new CommentsController();
        $productComments = $commentController->getByEntity('product', $product->getId());
        $messages = [];

        if (isset($_SESSION['messages'])) {
            $messages = $_SESSION['messages'];
            unset($_SESSION['messages']);
        }

        return [
            'template' => __DIR__ . '/Templates/Product.php',
            'request' => $request,
            'data'    => [
                'title' => $product->getTitle(),
                'page_title' => $product->getTitle(),
                'product' => $product,
                'comments' => $productComments,
                'messages' => $messages
            ]
        ];
    }

    /**
     * Generating list of all products
     *
     * @param array $request
     * @return array
     */
    public function PageProducts(array $request): array
    {
        $productController = new ProductController();
        $products = $productController->getAll();

        return [
            'template' => __DIR__ . '/Templates/Products.php',
            'request' => $request,
            'data'    => [
                'title' => self::WELCOME_TITLE,
                'page_title' => self::WELCOME_TITLE,
                'products' => $products,
                'messages' => []
            ]
        ];
    }

    /**
     * Storing products
     *
     * @param array $request
     * @return array
     */
    public function StoreProduct(array $request): array
    {
        $this->accountController->CheckPermissions('add_product', null, self::ADDING_PRODUCT_NOT_ALLOWED);
        $productController = new ProductController();
        $product = $productController->createProduct($request);

        $template = __DIR__ . '/Templates/ProductAdd.php';

        return [
            'template' => $template,
            'request' => $request,
            'data'    => [
                'title' => self::ADD_PRODUCT_TITLE,
                'page_title' => self::ADD_PRODUCT_TITLE,
                'fields' => $product->getMap(),
                'messages' => $productController->messages,
                'action_type' => 'store',
            ]
        ];
    }

    /**
     * Page with product form
     *
     * @param array $request
     * @return array
     */
    public function PageProductAdd(array $request): array
    {
        $this->accountController->CheckPermissions('add_product', null, self::ADDING_PRODUCT_NOT_ALLOWED);

        return [
            'template' => __DIR__ . '/Templates/ProductAdd.php',
            'request' => $request,
            'data'    => [
                'title' => self::ADD_PRODUCT_TITLE,
                'page_title' => self::ADD_PRODUCT_TITLE,
                'products' => [],
                'messages' => [],
                'action_type' => 'create',
            ]
        ];
    }
}