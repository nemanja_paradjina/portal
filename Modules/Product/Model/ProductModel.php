<?php


namespace Modules\Product\Model;

use Modules\Db\Database;

/**
 * Class ProductModel
 * @package Modules\Product\Model
 */
class ProductModel
{
    use \CreateTrait;

    public $required = [
        'create' => ['title']
    ];

    private $table = 'products';

    private $id;
    private $title;
    private $imageSource;
    private $imageResized;
    private $description;
    private $operation;

    private $map;

    /**
     * @return mixed
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * @param $operation
     */
    public function setOperation($operation)
    {
        $this->operation = $operation;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getImageSource()
    {
        return $this->imageSource;
    }

    /**
     * @param mixed $imageSource
     */
    public function setImageSource($imageSource): void
    {
        $this->imageSource = $imageSource;
    }

    /**
     * @return mixed
     */
    public function getImageResized()
    {
        return $this->imageResized;
    }

    /**
     * @param mixed $imageResized
     */
    public function setImageResized($imageResized): void
    {
        $this->imageResized = $imageResized;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }


    /**
     * Setter for data mapping
     */
    public function setMap(): void
    {
        $this->mapData();
    }

    /**
     * Returns product data map
     *
     * @return array
     */
    public function getMap(): array
    {
        return $this->map;
    }

    /**
     * Map request data field with model elements
     */
    protected function mapData(): void
    {
        $this->map = [
            'id'            => $this->getId(),
            'title'         => $this->getTitle(),
            'description'   => $this->getDescription(),
            'image_source'  => $this->getImageSource(),
            'image_resized' => $this->getImageResized(),
        ];
    }
}