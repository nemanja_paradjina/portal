<?php


namespace Modules\Product\Controllers;

use Modules\Db\Database;
use Modules\FileManager\FileHandlerTrait;
use Modules\Product\Controllers\Validators\ProductValidator;
use Modules\Product\Model\ProductModel;

/**
 * Class ProductController
 * @package Modules\Product\Controllers
 */
class ProductController
{
    use FileHandlerTrait;

    public $messages = [];

    /**
     * Method for creating new product
     *
     * @param array $request
     * @return ProductModel
     */
    public function createProduct(array $request): ProductModel
    {
        $product = $this->prepareData($request, 'create');
        $productValidator = new ProductValidator($product);
        $productValid = $productValidator->Valid();
        $files = [];
        if (true === $productValid) {
            $files = $this->HandleFiles($request);
            if (!empty($files)) {
                $product->setImageSource($files['image_source']);
                $product->setImageResized($files['image_resized']);
                $product->setMap();
            }
        }

        if (true === $productValid /*&& NULL !== $files*/) {
            $productId = $product->create();
            $product->setId($productId);

            if ($productId !== false) {
                redirect('/product/' . $productId);
                return [];
            }
        }

        $this->messages = $productValidator->getMessages();

        return $product;
    }

    /**
     * Returns all products
     */
    public function getAll()
    {
        $db = new Database();
        $data = $db->ExecuteGetQuery('SELECT * FROM products', []);
        $productExport = [];
        foreach ($data as $productArray) {
            $product = new ProductModel();
            $product->setId($productArray['id']);
            $product->setTitle($productArray['title']);
            $product->setImageResized($productArray['image_resized']);
            $product->setImageSource($productArray['image_source']);
            $product->setDescription($productArray['description']);
            $productExport[] = $product;
        }

        return $productExport;
    }

    public function getById(int $id): ProductModel
    {
        $database = new Database();
        $query = 'SELECT * FROM products WHERE id = ?';
        $data = $database->ExecuteGetQuery($query, [$id]);
        $product = new ProductModel();
        if (isset($data[0])) {
            $product->setId($data[0]['id']);
            $product->setTitle($data[0]['title']);
            $product->setDescription($data[0]['description']);
            $product->setImageSource($data[0]['image_source']);
            $product->setImageResized($data[0]['image_resized']);
            $product->setDescription($data[0]['description']);
            $product->setMap();
        }

        return $product;
    }


    /**
     * Mapping data to model
     *
     * @param array $request
     * @param string $operation
     *
     * @return ProductModel
     */
    private function prepareData(array $request, string $operation): ProductModel
    {
        $product = new ProductModel();
        $product->setTitle($request['title']);
        $product->setDescription($request['description']);
        $product->setOperation($operation);
        $product->setMap();

        return $product;
    }
}