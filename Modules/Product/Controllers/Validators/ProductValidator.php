<?php


namespace Modules\Product\Controllers\Validators;

use Modules\Db\Database;
use Modules\Product\Model\ProductModel;

/**
 * Class ProductValidator
 * @package Modules\Product\Controllers\Validators
 */
class ProductValidator
{
    private $required;
    private $data;
    private $messages = [];
    private $product;
    private $noPass = false;
    protected $database;

    public const PRODUCT_EXISTS = 'Product title must be unique. Please enter different title';

    public function __construct(ProductModel $product)
    {
        $this->product = $product;
        $this->required = $product->required[$product->getOperation()];
        $this->data = $product->getMap();

        $this->database = new Database();

        $this->ValidateRequired();
        $this->ValidateIfUnique();
    }

    /**
     * Returns information if model is valid
     *
     * @return bool
     */
    public function Valid(): bool
    {
        return !$this->noPass;
    }

    /**
     * Gets array of generated messages
     *
     * @return mixed
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

    /**
     * Checks if required fields are empty
     */
    private function ValidateRequired(): void
    {
        foreach ($this->data as $key => $field) {
            if (in_array($key, $this->required) && empty($field)) {
                $this->noPass = true;
                $this->messages['alert'][] = 'Field ' . $key . ' cannot be empty.';
            }
        }
    }

    /**
     * We shouldn't allow two identical products in a database
     */
    private function ValidateIfUnique(): void
    {
        $queryExtend = '';
        $parameters = [$this->data['title']];

//        if ($this->account->getOperation() == 'update') {
//            $queryExtend = 'AND id != ?';
//            array_push($parameters, $this->data['id']);
//        }

        $query = 'SELECT * FROM products WHERE title=? ' . $queryExtend;
        $users = $this->database->ExecuteGetQuery($query, $parameters);

        if (0 != count($users)) {
            $this->noPass = true;
            $this->messages['alert'][] = self::PRODUCT_EXISTS;
        }
    }
}