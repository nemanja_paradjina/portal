<?php

switch($templateData['data']['action_type']) {
    case 'create':
        $buttonValue = 'Create';
        $buttonName = 'create';
    break;
    default:
        $buttonValue = 'Update';
        $buttonName = 'update';
    break;
}

?>

<form method="post" action="<?php formAction(); ?>" enctype="multipart/form-data" class="edit-product-form">
    <div class="row">
        <div class="col-xs-12">
            <h2>Product Information</h2>
        </div>
        <div class="col-xs-6">
            <?php $FormBuilder->buildField('text', 'title', $templateData['data']['fields']['title'] ?? '', ['class' => 'form-control', 'required' => 'true'], 'Title'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <?php if (!empty($templateData['data']['fields']['image_resized'])): ?>
                <div class="form-group image-wrapper">
                    <label>Current Image</label>
                    <div>
                        <img class="table-image" src="<?php print $templateData['data']['fields']['image_resized']; ?>" />
                        <a href="/api/remove-image/<?php print $templateData['data']['fields']['id']; ?>" class="remove-image form-control btn btn-primary">Remove</a>
                    </div>
                </div>
            <?php endif; ?>

            <?php $FormBuilder->buildField('file', 'image_source', $templateData['data']['fields']['image_source'] ?? '', ['class' => 'form-control'], 'Product Image'); ?>
        </div>
        <div class="col-xs-12">
            <?php $FormBuilder->buildField('textarea', 'description', $templateData['data']['fields']['description'] ?? '', ['class' => 'form-control'], 'Product Description'); ?>
            <?php if ('update' == $templateData['data']['action_type']): ?>
                <?php $FormBuilder->buildField('hidden', 'product_id', $templateData['data']['fields']['id'], ['class' => 'form-control', 'required' => 'true'], ''); ?>
            <?php endif; ?>
        </div>
    </div>
    <?php $FormBuilder->buildField('submit', $buttonName, $buttonValue, ['class' => 'form-control btn btn-primary']); ?>
</form>