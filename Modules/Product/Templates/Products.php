<?php
$counter = 1;
$perRow = 3;
$maxProducts = count($templateData['data']['products']);
?>

<?php  if ($permissionManager->can('add_product')):  ?>
    <a href="/product/add" class="btn btn-primary link-button add-button">Add Product</a>
<?php endif; ?>


<?php foreach($templateData['data']['products'] as $product): ?>
<?php if (1 == $counter):?>
<div class="row">
    <?php endif; ?>

    <div class="col-sm-4 profile-thumb-wrapper">
        <a href="/product/<?php print $product->getId(); ?>">
            <div class="product-image-wrapper">
                <img src="<?php print $product->getImageResized(); ?>" />
            </div>
            <div class="product-title">
                <h3><?php print $product->getTitle(); ?></h3>
            </div>
        </a>
        <div class="product-description">
            <?php print $product->getDescription(); ?>
        </div>
    </div>

    <?php if (0 == $counter % $perRow && $counter != $maxProducts): ?>
</div><div class="row">
    <?php endif; ?>
    <?php $counter++; ?>
    <?php endforeach; ?>
</div>