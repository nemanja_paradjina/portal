<?php
    $commentEntityType = 'product';
    $commentEntityId   = $templateData['data']['product']->getId();
?>

<div class="row">
    <div class="col-sm-12 profile-data">
        <?php if (isset($templateData['data']['product'])): ?>
            <table class="table">
                <tr>
                    <th scope="row">Image</th>
                    <td><img class="table-image" src="<?php print $templateData['data']['product']->getImageSource(); ?>" /></td>
                </tr>
                <tr>
                    <th scope="row">Description</th>
                    <td><?php print $templateData['data']['product']->getDescription(); ?></td>
                </tr>
            </table>
            <div class="comment-list-wrapper">
                <?php require_once $_SERVER['DOCUMENT_ROOT'] . '/../Modules/Comments/Templates/CommentList.php'; ?>
            </div>
            <div class="comment-form">
                <?php require_once $_SERVER['DOCUMENT_ROOT'] . '/../Modules/Comments/Templates/CommentForm.php'; ?>
            </div>
        <?php endif; ?>
    </div>
</div>