<?php

/**
 * Debugging helper
 *
 * @param $param
 */
function dd($param)
{
    die(var_dump($param));
}

/**
 * Loading theme path
 *
 * @return string
 */
function getThemeDir(): string
{
    return $_SERVER['DOCUMENT_ROOT'] . '/../Themes/' . DEFAULT_THEME;
}

/**
 * Generating action for forms
 */
function formAction()
{
    echo $_SERVER['REQUEST_URI'];
}

function Redirect(string $url)
{
    header('Location: ' . $url);
    die();
}

function getTitle(string $title): string
{
    return $title . ' | ' . SITE_TITLE;
}
