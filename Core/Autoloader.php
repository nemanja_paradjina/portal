<?php

/**
 * Class Autoloader
 */
class Autoloader
{
    private $excluded = [];

    public function __construct()
    {
        $this->excluded[] = 'Templates';
    }

    /**
     * Recursively load all module files
     *
     * @param string $path
     *
     * @return array
     */
    public function Load(string $path)
    {
        return $this->LoadClasses($path);
    }

    /**
     * Forming an array of required classes
     *
     * @param string $path
     * @param array $loadedPaths
     *
     * @return array
     */
    private function LoadClasses(string $path, ?array $loadedPaths = []): array
    {
        $ModuleDirectory = array_values(array_diff(scandir($path), array('..', '.')));

        if (count($ModuleDirectory ) > 0) {
            foreach ($ModuleDirectory as $ModuleInfo) {
                if (!in_array($ModuleInfo, $this->excluded)) {
                    if (!is_dir($path . '/' . $ModuleInfo)) {
                        $loadedPaths[] = $path . '/' . $ModuleInfo;
                    }

                    if(is_dir($path . '/' . $ModuleInfo)) {
                        $loadedPaths = $this->LoadClasses($path . '/' . $ModuleInfo, $loadedPaths);
                    }
                }
            }
        }
        return $loadedPaths;
    }
}
