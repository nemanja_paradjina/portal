<?php
ini_set('display_errors', DEBUG);
ini_set('display_startup_errors', DEBUG);
error_reporting(E_ALL);
/**
 * Registering global methods across the portal
 */
require_once __DIR__ . '/Globals.php';

/**
 * Autoloading classes
 */
require_once __DIR__ . '/Autoloader.php';

/**
 * Registering classes
 */
spl_autoload_register(function() {
    $Autoloader = new Autoloader();
    $modules = $Autoloader->Load(__DIR__ . '/../Modules');
    foreach ($modules as $module) {
        require_once $module;
    }
});

/**
 * Controlling user session
 */
session_start();

use Modules\RequestHandler;
use Modules\FormBuilder;
use Modules\PermissionManager;

$FormBuilder = new FormBuilder();
$RequestHandler = new RequestHandler();
$permissionManager = new PermissionManager();
$templateData = $RequestHandler->Resolve();

if (!$RequestHandler->isApi()) {
    $messages = $templateData['data']['messages'];
    require getThemeDir() . '/Theme.php';
} else {
    //Set the Content-Type header to application/json.
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    echo $templateData;
}


