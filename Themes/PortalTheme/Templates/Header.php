<header>
    <div class="container">
        <div class="row">
            <div class="col-sm-2">
                <a class="logo" href="/">PORTAL</a>
            </div>
            <div class="col-sm-10">
                <?php require_once getThemeDir() . '/Templates/Menu.php'; ?>
            </div>
        </div>
    </div>
</header>