<?php if (!empty($messages['alert']) && count($messages['alert']) > 0): ?>
    <?php foreach($messages['alert'] as $message): ?>
        <div class="alert alert-warning fade in alert-dismissible show"><?php print $message; ?></div>
    <?php endforeach; ?>
<?php endif; ?>
<?php if (isset($messages['info']) && count($messages['info']) > 0): ?>
    <?php foreach($messages['info'] as $message):?>
        <div class="alert alert-info"><?php print $message; ?></div>
    <?php endforeach; ?>
<?php endif; ?>
<?php if (isset($messages['success']) && count($messages['success']) > 0): ?>
    <?php foreach($messages['success'] as $message):?>
        <div class="alert alert-success"><?php print $message; ?></div>
    <?php endforeach; ?>
<?php endif; ?>
