<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <?php require_once getThemeDir() . '/Templates/Tools/Messages.php'; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $templateData['data']['page_title']; ?></h1>
                <?php if (!empty($templateData['template'])): ?>
                    <?php require_once $templateData['template']; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
