<body>
    <?php require_once getThemeDir() . '/Templates/Header.php'; ?>
    <?php require_once getThemeDir() . '/Templates/Content.php'; ?>
    <?php require_once getThemeDir() . '/Templates/Footer.php'; ?>
    <?php require_once getThemeDir() . '/Templates/Scripts.php'; ?>
</body>
