<nav class="nav">
    <?php if ($permissionManager->can('edit_comments')): ?>
        <a class="nav-link" href="/comments">Comments</a>
    <?php endif; ?>

    <?php if ($permissionManager->can('list_profiles')): ?>
        <a class="nav-link" href="/account/list">Profiles</a>
    <?php endif; ?>

    <?php if ($permissionManager->can('create_account')): ?>
        <a class="nav-link" href="/account/register">Register</a>
    <?php endif; ?>

    <?php if ($permissionManager->can('view_own_profile')): ?>
        <a class="nav-link" href="/account/<?php print $_SESSION['account']->getId(); ?>">My Account</a>
    <?php else: ?>
        <a class="nav-link" href="/account/login">Login</a>
    <?php endif; ?>

    <?php if ($permissionManager->can('logout')): ?>
        <a class="nav-link" href="/account/logout">Logout</a>
    <?php endif; ?>
</nav>