(($) => {
    $.fn.extend({
        CommentPlugin: function() {

            /**
             * Hardcoded values
             * @type {{}}
             */
            this.Init = {

            };

            /**
             * Use to define all plugin selectors
             */
            this.Selector = {
                CommentTogglerBtn:      '.comment-list .btn-toggler',
            };

            /**
             * Use to initiate selected objects
             */
            this.JqueryObject = {
                BtnToggler:       $(this.Selector.CommentTogglerBtn),
            };

            /**
             * Used to register object methods
             */
            this.Methods = {
                CommentToggler: function(e) {
                    e.preventDefault();
                    let href = $(this).attr('href');
                    let hrefSeparator = href.split('/');
                    let id = hrefSeparator[hrefSeparator.length - 1];

                    let action = 'disable';
                    if ($(this).hasClass('btn-disabled')) {
                        action = 'enable';
                    }

                    $.ajax({
                        method: "POST",
                        url: href,
                        data: { id: id, action: action }
                    }).done((data) => {
                        if (data.success == true) {
                            let disabledClasses = 'btn-disabled info-style';
                            let enabledClasses  = 'btn-enabled important-style';
                            if (action == 'enable') {
                                $('.comment-list #btn-toggler-' + id).text('Un-publish')
                                    .removeClass(disabledClasses)
                                    .addClass(enabledClasses);
                            } else {
                                $('.comment-list #btn-toggler-' + id).text('Publish')
                                    .removeClass(enabledClasses)
                                    .addClass(disabledClasses);
                            }
                        }
                    });
                },
            };

            /**
             * Re-usable logical methods (do not mix this with general methods
             * function
             *
             * @type {{}}
             */
            this.Services = {

            };

            /**
             * Assigning events
             * @constructor
             */
            this.RegisterEvents = function() {
                this.JqueryObject.BtnToggler.click(this.Methods.CommentToggler);
            };

            /**
             * Initiating plugin events
             */
            this.RegisterEvents();

            return this;
        }
    });

    $(document).ready(() => {
        $('.table.comment-list').CommentPlugin();
    });
})(jQuery);
