INSTALLATION:

    1. Setup config.php file (database details should be changed)
    2. Make sure that you have installed ImageMagick library on server
       - apt-get install php-imagick 
       - Add "extension=imagick.so" to php.ini)
    3. Point Apache virtual host directory to "/public" directory
    4. Import database
    5. Run npm install
    6. Setup Email server, either you can use gmail or mailhog docker file (execute docker-compose up inside docker directory - http://localhost:8025/)
       NOTE: I included both setups, just change the data in config
    7. PHP minimum version - 7.1

INSTRUCTIONS:

    - Root of this website is /public folder
    - To work on the theme modifications you can use gulp
    - Command "gulp prod" generates production minified files in public directory
    - Command "gulp watch" will create non minified files during development
    - Scripts will be loaded based on the ENV settings in Config.php 
    - Development environments are: Themes/PortalTheme and Modules directory
    - Javascript files are automatically loaded from folder jsPlugins and compiled via gulp
    - Module structure:
        - Controllers
        - Models
        - Templates
        - MainModuleFile.php (which has methods that are defined in Routes file)
        - Routes.php (if routes are needed)
        (Not all of this files are necessary, whatever you put it will be autoloaded and available)
    - Libraries:
        - Only used PHPMailer and node_modules
    
Built by Nemanja Paradjina:

    - Everything except script for cropping images (found on google canvas aspect ratio, called it: getThumbnailAspectRatio)
    - I had gulp file before I started this small project (also created by me, inspired by extjs in Shopware)

WHAT IF:

    - We need a new page?
        - Create new Module or update existing one
        - Create route member (see account for example, file Routes)
        - Define new array path -> request-method -> class -> class-method
        - Create defined class and method
        - Add return array which contains information about the template, fields, page messages etc (see account for example)
    - We need to style a theme
        - Install npm packages (npm install and install requirements if any)
        - Execute gulp watch
        - Make sure that config file has env set for development
        - Modify scss file and see as the watcher renders it
        - After work is complete and files must be committed to production execute 'gulp prod' (generates minified files)
    
AUTHOR: Nemanja Paradjina

Administration credentials (username / pass):
admin / admin