var gulp = require('gulp'),
    livereload = require('gulp-livereload'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    fs = require('fs')
    minify = require('gulp-minify');

var theme = 'Themes/PortalTheme';
var jsFiles = [];
fs.readdirSync('./' + theme + '/jsPlugins').forEach(file => {
    jsFiles.push('./' + theme + '/jsPlugins/' + file);
});

/**
 * Compiling production scss files
 */
gulp.task('sass', done => {
    gulp.src(theme + '/Sass/Style.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer('last 2 version'))
        .pipe(gulp.dest('Public/Css'));
    done();
});

/**
 * Compiling dev scss files
 */
gulp.task('sassDev', done => {
    gulp.src(theme + '/Sass/Style.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer('last 2 version'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('Public/Css'));
    done();
});

/**
 * Compiling production javascript files
 */
gulp.task('js', done => {
    gulp.src(jsFiles)
        .pipe(concat('Script.js'))
        .pipe(minify())
        .pipe(gulp.dest('Public/Js'));
    done();
});

/**
 * Compiling development javascript files
 */
gulp.task('jsDev', done => {
    gulp.src(jsFiles)
        .pipe(concat('Script.js'))
        .pipe(gulp.dest('Public/Js'));
    done();
});

/**
 * Definition of watcher
 */
gulp.task('watch', done => {
    livereload.listen();
    gulp.watch(theme + '/Sass/*.scss', gulp.series('sassDev'));
    gulp.watch(['Public/Css/Style.css'], function (files) {
        livereload.changed(files)
    });
    gulp.watch(jsFiles, gulp.series('jsDev')).on('change', livereload);
    done();
});

gulp.task('prod', gulp.series('sass', 'js'));
